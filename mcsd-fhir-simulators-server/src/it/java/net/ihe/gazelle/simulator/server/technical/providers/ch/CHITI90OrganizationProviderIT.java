package net.ihe.gazelle.simulator.server.technical.providers.ch;


import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Organization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@QuarkusTest
@QuarkusTestResource(ProviderTestResource.class)
class CHITI90OrganizationProviderIT {

    private FhirResourceParser parser;

    @BeforeEach
    void setUp(){
        parser = new FhirResourceParser();
    }


    @Test
    void testRead() {
        String body =  given()
            .when()
            .get("mcsd-simulator/fhir/ch/Organization/Connectathon-NGO-Two")
            .then()
            .statusCode(200)
            .extract()
            .body()
            .asString()
        ;

        Organization organization = parser.deserialize(body, Organization.class);

        assertEquals("Organization/Connectathon-NGO-Two", organization.getId());
        assertEquals("Connectathon CSD NGO Two", organization.getName());
        assertFalse(organization.getActive());

    }

    @Test
    void testSearch() {
        String body =  given()
            .when()
            .get("mcsd-simulator/fhir/ch/Organization?name=Connectathon CSD NGO Two")
            .then()
            .statusCode(200)
            .extract()
            .body()
            .asString()
        ;

        Bundle bundle = parser.deserialize(body, Bundle.class);
        assertEquals(1, bundle.getTotal());

        Organization organization = (Organization) bundle.getEntry().get(0).getResource();

        assertEquals("Connectathon-NGO-Two", organization.getIdPart());
        assertEquals("Connectathon CSD NGO Two", organization.getName());
        assertFalse(organization.getActive());

    }

}

package net.ihe.gazelle.simulator.server.technical.providers.ch;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.capitalize;

public class ProviderTestResource implements QuarkusTestResourceLifecycleManager {


    private final ObjectMapper objectMapper = new ObjectMapper();

    private final List<String> resourcesTypes = List.of("organizations");

    private final Map<String, String> resourceMap = java.util.Map.of(
            "organizations", "Organization"
    );

    private final String tmpMcsdDirectory = "/tmp/tempMcsdDirectory";

    @Override
    public Map<String, String> start() {
        String resourcePath = ProviderTestResource.class.getClassLoader().getResource("/mcsd-simulator").getPath();
        ArrayNode resources = objectMapper.createArrayNode();
        for(String resourceType : resourcesTypes){
            Path resourcesDirectory = Path.of(resourcePath, resourceType);
            JsonNode node = objectMapper.createObjectNode()
                    .put("resourceType", resourceMap.get(resourceType))
                    .put("resourcesDirectory", resourcesDirectory.toString())
                    ;
            resources.add(node);
        }
        try {
            File tmpMcsdDirectoryFile = new File(tmpMcsdDirectory);
            if (!tmpMcsdDirectoryFile.exists()) {
                tmpMcsdDirectoryFile.mkdirs();
            }
            objectMapper.writeValue(Path.of(tmpMcsdDirectory, "resourcesSummary.json").toFile(), resources);
            return Map.of(
                    "mcsd.server.validation.enabled", "false",
                    "resources.summary.path", tmpMcsdDirectory+"/resourcesSummary.json"
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void stop() {

    }

}

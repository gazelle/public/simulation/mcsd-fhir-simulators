package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class EndpointResourceIndexerTest {

    static String resourceEndpoint = """
            {
              "resourceType": "Endpoint",
              "identifier": [
                {
                  "system": "http://example.com/endpoints",
                  "value": "12345"
                }
              ],
              "managingOrganization": {
                "reference": "Organization/ABCHealthcare"
              },
              "status": "active"
            }
            """;

    static String resourceEndpoint2 = """
            {
              "resourceType": "Endpoint",
              "identifier": [
                {
                  "system": "http://example.com/endpoints",
                  "value": "82348"
                }
              ],
              "managingOrganization": {
                "reference": "Organization/ABCHealthcare"
              },
              "status": "active"
            }
            """;

    static List<String> listEndpoint = new ArrayList<>();
    static FhirResourceParser parser = new FhirResourceParser();
    static FhirResourceIndexer indexer;


    @BeforeAll
    static void setUp() {
        indexer = new EndpointResourceIndexer();
        listEndpoint.add(resourceEndpoint);
        listEndpoint.add(resourceEndpoint2);

    }
    @Test
    void createIndexes() {
        assertDoesNotThrow(() -> indexer.createIndexes(listEndpoint));
    }

    @Test
    void getAnalyzer() {
        assertNotNull(indexer.getAnalyzer());
    }

    @Test
    void getDirectory() {
        assertNotNull(indexer.getDirectory());
    }


    @Test
    void getMapOfIndexes() {
        assertNotNull(indexer.getMapOfIndexes());
    }
}
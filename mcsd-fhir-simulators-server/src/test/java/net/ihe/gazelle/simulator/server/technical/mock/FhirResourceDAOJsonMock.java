package net.ihe.gazelle.simulator.server.technical.mock;

import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Resource;

import java.util.List;
import java.util.Map;

public class FhirResourceDAOJsonMock implements ResourceServiceDAO {
    @Override
    public Resource getResourceWithId(String id) {
        if (id == null || id.isBlank() || !id.equals("test")) {
            return null;
        } else {
            Practitioner practitioner = new Practitioner();
            practitioner.setId("test");
            return practitioner;
        }
    }

    @Override
    public List<IBaseResource> getAllResourcesByResourceType(String resourceType) {
        if ("Practitioner".equals(resourceType)) {
            return List.of(new Practitioner());
        }
        return List.of();
    }

    @Override
    public Map<String, String> getIndexes() {
        return Map.of("_test","practitioner_test");
    }

    @Override
    public void queryResourceByExactString(String index, String name) throws FhirResourceSearcherException {
        if ("exception".equals(index)){
            throw new FhirResourceSearcherException("");
        }
    }

    @Override
    public void queryResourceByApproximateString(String index, String name) throws FhirResourceSearcherException {

    }

    @Override
    public void queryResourceByPrefix(String index, String name) {

    }

    @Override
    public void queryResourceByDate(String index, long queryMin, long queryMax) {

    }

    @Override
    public List<IBaseResource> getResourcesFromSearch(String resourceName) throws FhirResourceSearcherException {
        if("exception".equals(resourceName)){
            throw new FhirResourceSearcherException("");
        } else return List.of(new Practitioner());
    }
}

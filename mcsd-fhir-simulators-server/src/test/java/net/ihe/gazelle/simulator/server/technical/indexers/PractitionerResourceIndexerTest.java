package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Practitioner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PractitionerResourceIndexerTest {

    static String resource = """
            {
              "resourceType" : "Practitioner",
              "id" : "2d12b2fd-bddf-49be-9d1d-dc7084168cd8",
              "meta" : {
                "profile" : ["http://fhir.ch/ig/ch-epr-fhir/StructureDefinition/CH.mCSD.Practitioner"]
              },
              "identifier" : [{
                "system" : "urn:oid:2.51.1.3",
                "value" : "7601000194633"
              },
              {
                "value" : "uid=CommunityA:11000003000,OU=HCProfessional,DC=HPD,O=BAG,C=ch"
              }],
              "active" : true,
              "name" : [{
                "text" : "Hans Meier",
                "family" : "Meier",
                "given" : ["Hans"],
                "prefix" : ["Dr."]
              }],
              "telecom" : [{
                "system" : "email",
                "value" : "drhmeier@email.com"
              },
              {
                "system" : "phone",
                "value" : "+41 32 001 00 81"
              }],
              "gender" : "male",
              "qualification" : [{
                "code" : {
                  "coding" : [{
                    "system" : "http://snomed.info/sct",
                    "code" : "309343006",
                    "display" : "Physician"
                  }]
                }
              },
              {
                "code" : {
                  "coding" : [{
                    "system" : "urn:oid:2.16.756.5.30.1.127.3.5",
                    "code" : "1051",
                    "display" : "General internal medicine"
                  }]
                }
              }],
              "communication" : [{
                "coding" : [{
                  "system" : "urn:ietf:bcp:47",
                  "code" : "de"
                }]
              }]
            }
            """;


    static List<String> list = new ArrayList<>();
    static FhirResourceParser parser = new FhirResourceParser();
    static FhirResourceIndexer indexer;


    @BeforeAll
    static void setUp() {
        indexer = new PractitionerResourceIndexer();
        list.add(resource);
    }

    @Test
    void createIndexes() {
        assertDoesNotThrow(() -> indexer.createIndexes(list));
    }

    @Test
    void getAnalyzer() {
        assertNotNull(indexer.getAnalyzer());
    }

    @Test
    void getDirectory() {
        assertNotNull(indexer.getDirectory());
    }


    @Test
    void getMapOfIndexes() {
        assertNotNull(indexer.getMapOfIndexes());
    }
}
package net.ihe.gazelle.simulator.server.technical.io.mapper;

import net.ihe.gazelle.simulator.server.business.config.ValidationConfig;

public class ValidationConfigMock implements ValidationConfig {



    @Override
    public String getHttpIti90SearchPractitioner() {
        return "search-practitioner";
    }

    @Override
    public String getHttpIti90SearchPractitionerRole() {
        return "search-practitioner-role";
    }

    @Override
    public String getHttpIti90SearchOrganization() {
        return "search-organization";
    }

    @Override
    public String getHttpIti90Read() {
        return "read";
    }

    @Override
    public String getHttpValidatorName() {
        return "validator-name";
    }

    @Override
    public boolean isValidationEnabled() {
        return true;
    }
}

package net.ihe.gazelle.simulator.server.technical.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MPValidationConfigCHTest {

    private MPValidationConfigCH mPValidationConfigCH;

    @BeforeEach
    public void setUp() {
        mPValidationConfigCH = new MPValidationConfigCH();
    }

    @Test
    void testGetHttpIti90SearchPractitioner() {
        String value = mPValidationConfigCH.getHttpIti90SearchPractitioner();
        assertEquals("mcsd.server.validation.profile-iti-90-practitioner", value);
    }

    @Test
    void testGetHttpIti90SearchPractitionerRole() {
        String value = mPValidationConfigCH.getHttpIti90SearchPractitionerRole();
        assertEquals("mcsd.server.validation.profile-iti-90-practitionerRole", value);
    }

    @Test
    void testGetHttpIti90SearchOrganization() {
        String value = mPValidationConfigCH.getHttpIti90SearchOrganization();
        assertEquals("mcsd.server.validation.profile-iti-90-organization", value);
    }

    @Test
    void testGetHttpValidatorName() {
        String value = mPValidationConfigCH.getHttpValidatorName();
        assertEquals("mcsd.server.validation.name", value);
    }

    @Test
    void testGetHttpIti90Read() {
        String value = mPValidationConfigCH.getHttpIti90Read();
        assertEquals("mcsd.server.validation.profile-iti-90-read", value);
    }

    @Test
    void testIsValidationEnabled() {
        boolean value = mPValidationConfigCH.isValidationEnabled();
        assertTrue(value);
    }


}

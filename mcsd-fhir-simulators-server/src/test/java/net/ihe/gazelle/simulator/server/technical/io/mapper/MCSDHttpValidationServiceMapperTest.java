package net.ihe.gazelle.simulator.server.technical.io.mapper;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MCSDHttpValidationServiceMapperTest {


    private MCSDHttpValidationServiceMapper mCSDHttpValidationServiceMapper;

    @BeforeEach
    public void setUp() {
        mCSDHttpValidationServiceMapper = new MCSDHttpValidationServiceMapper(new ValidationConfigMock());
    }


    @ParameterizedTest
    @MethodSource("provideParametersForValidationService")
    void testGetValidationService(String resourceName, String expectedValidator, RestOperationTypeEnum operationType){

        RequestDetails requestDetails = mock(RequestDetails.class);
        when(requestDetails.getRestOperationType()).thenReturn(operationType);
        when(requestDetails.getResourceName()).thenReturn(resourceName);

        ValidationService validationService = mCSDHttpValidationServiceMapper.getValidationService(requestDetails);

        assertNotNull(validationService);
        assertEquals("validator-name", validationService.getName());
        assertEquals(expectedValidator, validationService.getValidator());
    }

    private static Stream<Arguments> provideParametersForValidationService() {
        return Stream.of(
                Arguments.of("Practitioner", "search-practitioner", RestOperationTypeEnum.SEARCH_TYPE),
                Arguments.of("PractitionerRole", "search-practitioner-role", RestOperationTypeEnum.SEARCH_TYPE),
                Arguments.of("Organization", "search-organization", RestOperationTypeEnum.SEARCH_TYPE),
                Arguments.of(null, "read", RestOperationTypeEnum.READ)
        );
    }

    @Test
    void testGetValidationServiceForUnsupportedResourceType(){

        RequestDetails requestDetails = mock(RequestDetails.class);
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.SEARCH_TYPE);
        when(requestDetails.getResourceName()).thenReturn("UnsupportedResourceType");

        assertNull(mCSDHttpValidationServiceMapper.getValidationService(requestDetails));
    }

    @Test
    void testGetValidationServiceForUnsupportedOperationType(){

        RequestDetails requestDetails = mock(RequestDetails.class);
        when(requestDetails.getRestOperationType()).thenReturn(RestOperationTypeEnum.DELETE);

        assertNull(mCSDHttpValidationServiceMapper.getValidationService(requestDetails));
    }


}

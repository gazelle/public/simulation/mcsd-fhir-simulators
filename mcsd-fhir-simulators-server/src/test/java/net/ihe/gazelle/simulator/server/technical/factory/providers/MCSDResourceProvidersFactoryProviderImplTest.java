package net.ihe.gazelle.simulator.server.technical.factory.providers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

class MCSDResourceProvidersFactoryProviderImplTest {

    @Test
    void createListOfMCSDResourceProviders() {
        MCSDResourceProvidersFactoryProviderImpl factoryProvider = new MCSDResourceProvidersFactoryProviderImpl();
        assertFalse(factoryProvider.createListOfMCSDResourceProviders("IHE").isEmpty());
        assertFalse(factoryProvider.createListOfMCSDResourceProviders("CH").isEmpty());
    }

}
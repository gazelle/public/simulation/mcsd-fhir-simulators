package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Organization;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class OrganizationResourceIndexerTest {

    static String organization = """
            <Organization xmlns="http://hl7.org/fhir">
              <!-- Organization for HPD testing, in mCSD format -->
              <id value="Connecthon-Mercy-Hospital"/>
              <meta>
                <profile value="http://ihe.net/fhir/StructureDefinition/IHE_mCSD_Organization"/>
                <lastUpdated value="2024-05-05T14:00:00Z"/>
              </meta>
              <identifier>
                <use value="official"/>
                <system value="http://connectathon.ihe"/>
                <value value="11"/>
              </identifier>
              <active value="true"/>
              <name value="Mercy Philadelphia Hospital"/>
              <alias value="Mercy Health Center"/>
            </Organization>
            """;


    static List<String> list = new ArrayList<>();
    static FhirResourceParser parser = new FhirResourceParser();
    static FhirResourceIndexer indexer;


    @BeforeAll
    static void setUp() {
        indexer = new OrganizationResourceIndexer();
        list.add(organization);
    }

    @Test
    void createIndexes() {
        assertDoesNotThrow(() -> indexer.createIndexes(list));
    }

    @Test
    void getAnalyzer() {
        assertNotNull(indexer.getAnalyzer());
    }

    @Test
    void getDirectory() {
        assertNotNull(indexer.getDirectory());
    }


    @Test
    void getMapOfIndexes() {
        assertNotNull(indexer.getMapOfIndexes());
    }
}
package net.ihe.gazelle.simulator.server.technical.io;

import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceParserException;
import org.hl7.fhir.r4.model.Practitioner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FhirResourceParserTest {

    static FhirResourceParser parser;
    static Practitioner practitioner;
    static String jsonPractitioner = """
            {"resourceType":"Practitioner","id":"123"}""";

    static String xmlPractitioner = """
            <Practitioner xmlns="http://hl7.org/fhir"><id value="123"/></Practitioner>""";

    static String fshPractitioner = """
             // Define a Practitioner resource
             Profile: MyPractitioner
             Parent: Practitioner
            
             Instance: MyPractitionerInstance
             InstanceOf: MyPractitioner
             * id = "123"
            """;


    @BeforeAll
    static void init() {
        parser = new FhirResourceParser();
    }

    @Test
    void serializeJson() {
        //Given
        practitioner = new Practitioner();
        practitioner.setId("123");
        //When
        String jsonPractitionerToTest = parser.serialize(practitioner, "json");
        //Then
        assertEquals(jsonPractitioner, jsonPractitionerToTest);
    }

    @Test
    void serializeXml() {
        //Given
        practitioner = new Practitioner();
        practitioner.setId("123");
        //When
        String xmlPractitionerToTest = parser.serialize(practitioner, "xml");
        //Then
        assertEquals(xmlPractitioner, xmlPractitionerToTest);

    }

    @Test
    void serializeFailed() {
        assertThrows(FhirResourceParserException.class, () -> parser.serialize(practitioner, "txt"));
    }


    @Test
    void deserializeJson() {
        //When
        practitioner = (Practitioner) parser.deserialize(jsonPractitioner, Practitioner.class);
        //Then
        assertEquals("Practitioner/123", practitioner.getId());
    }

    @Test
    void deserializeXml() {
        //When
        practitioner = (Practitioner) parser.deserialize(xmlPractitioner, Practitioner.class);
        //Then
        assertEquals("Practitioner/123", practitioner.getId());
    }

    @Test
    void deserializeFailed() {
        //Then
        assertThrows(FhirResourceParserException.class, () -> parser.deserialize(fshPractitioner, Practitioner.class));
    }
}
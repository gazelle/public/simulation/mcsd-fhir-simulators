package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PractitionerRoleResourceIndexerTest {

    static String resource = """
            {
              "resourceType": "PractitionerRole",
              "id": "PeterPanPraxisP",
              "meta": {
                "profile": [
                  "http://fhir.ch/ig/ch-epr-mhealth/StructureDefinition/CH.mCSD.PractitionerRole"
                ]
              },
              "identifier": [
                {
                  "value": "CN=CommunityA:00000001001,OU=Relationship,DC=HPD,O=BAG,C=ch"
                }
              ],
              "active": true,
              "practitioner": {
                "reference": "Practitioner/DrPeterPan"
              },
              "organization": {
                "reference": "Organization/PraxisP"
              },
              "code": [
                {
                  "coding": [
                    {
                      "system": "http://snomed.info/sct",
                      "code": "309343006",
                      "display": "Physician"
                    }
                  ]
                }
              ],
              "specialty": [
                {
                  "coding": [
                    {
                      "system": "urn:oid:2.16.756.5.30.1.127.3.5",
                      "code": "1051",
                      "display": "General internal medicine"
                    }
                  ]
                }
              ]
            }
            """;


    static List<IBaseResource> list = new ArrayList<>();
    static FhirResourceParser parser = new FhirResourceParser();
    static FhirResourceIndexer indexer;


    @BeforeAll
    static void setUp() {
        indexer = new PractitionerRoleResourceIndexer();
        list.add(parser.deserialize(resource, PractitionerRole.class));
    }

    @Test
    void createIndexes() {
        assertDoesNotThrow(() -> indexer.createIndexes(List.of(resource)));
    }

    @Test
    void getAnalyzer() {
        assertNotNull(indexer.getAnalyzer());
    }

    @Test
    void getDirectory() {
        assertNotNull(indexer.getDirectory());
    }


    @Test
    void getMapOfIndexes() {
        assertNotNull(indexer.getMapOfIndexes());
    }
}
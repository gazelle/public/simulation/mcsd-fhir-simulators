package net.ihe.gazelle.simulator.server.technical.providers.ch;

import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Organization;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CHITI90OrganizationProviderTest {

    @Mock
    private FhirResourceService service;

    @InjectMocks
    CHITI90OrganizationProvider provider = new CHITI90OrganizationProvider();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }



    @Test
    void testGetResourceType() {
        assertEquals(Organization.class, provider.getResourceType());
    }

    @Test
    void testOKRead() {
        IdType idType = new IdType(1);
        //When
        when(service.getResourceById(any())).thenReturn(new Organization().setId("1"));
        IBaseResource resource = provider.read(idType);
        //Then
        assertEquals("1", resource.getIdElement().getIdPart());
    }

    @Test
    void testKORead() {
            //Given
            IdType idType = new IdType(1);
            //When
            //Then
            Exception exception = assertThrows(ResourceNotFoundException.class, () -> provider.read(idType));
            assertEquals(new ResourceNotFoundException(idType).getMessage(), exception.getMessage());
    }

    @Test
    void searchOrganizationWithParams() throws FhirResourceServiceException {
            //When
            List<IBaseResource> list = provider.searchOrganization(new TokenParam(),new DateRangeParam(), new StringParam(), new TokenParam(),new TokenParam(),new ReferenceParam(), new TokenParam());
            //Then
            assertNotNull(list);
    }

    @Test
    void searchOrganizationWithNull() throws FhirResourceServiceException {
            //When
            List<IBaseResource> list = provider.searchOrganization(null,null, null, null,null,null, null);
            //Then
            assertNotNull(list);
    }
}
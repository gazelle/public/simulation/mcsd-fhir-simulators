package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.StoredFields;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TotalHits;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class FhirResourceSearchProcessorServiceTest {

    FhirResourceIndexer indexer = mock(FhirResourceIndexer.class);
    FhirResourceSearchProcessorService searchService;
    Map<String,String> map = Map.of("_name","practitioner_name",
            "_date","practitioner_date",
            "_name_ic","practitioner_name_ic",
            "_resource","practitioner_resource");
    String practitionerS1 = """
                {"resourceType":"Practitioner","id":"123"}
                """;

    @BeforeEach
    void setUp() {
        doReturn(map).when(indexer).getMapOfIndexes();
        searchService = new FhirResourceSearchProcessorService(indexer);
    }

    @Test
    void exactSearch() {
        searchService.exactSearch("_name","toto");
        assertEquals(1,searchService.queryBuilder.build().clauses().size());
    }

    @Test
    void longNumberSearch() {
        searchService.longNumberSearch("_date",0L,Long.MAX_VALUE);
        assertEquals(1,searchService.queryBuilder.build().clauses().size());
    }

    @Test
    void prefixSearch() {
        searchService.prefixSearch("_name","toto");
        assertEquals(1,searchService.queryBuilder.build().clauses().size());
    }

    @Test
    void prefixSearchWithNull() {
        searchService.prefixSearch("_name",null);
        assertEquals(1,searchService.queryBuilder.build().clauses().size());
    }

    @Test
    void approximateSearch() {
        searchService.approximateSearch("_name","toto");
        assertEquals(1,searchService.queryBuilder.build().clauses().size());
    }

    @Test
    void approximateSearchWithNull() {
        searchService.approximateSearch("_name",null);
        assertEquals(1,searchService.queryBuilder.build().clauses().size());
    }

    @Test
    void getMatchingResourcesAsString() throws IOException, FhirResourceSearcherException {
        //Given
        IndexSearcher mockedSearcher = mock(IndexSearcher.class);
        searchService.searcher = mockedSearcher;
        Document document = new Document();
        document.add(new TextField("_resource", practitionerS1, Field.Store.YES));
        ScoreDoc scoreDoc = new ScoreDoc(1,1,1);
        TotalHits totalHits = new TotalHits(1L, TotalHits.Relation.EQUAL_TO);
        TopDocs hits = new TopDocs(totalHits,new ScoreDoc[]{scoreDoc});
        StoredFields mockedStoreFields =  mock(StoredFields.class);

        //When
        doReturn(hits).when(mockedSearcher).search(any(),anyInt());
        doReturn(mockedStoreFields).when(mockedSearcher).storedFields();
        List<String> results = searchService.getMatchingResourcesAsString();

        //Then
        assertEquals(1,results.size());

    }

    @Test
    void getMatchingResourcesAsStringThrows() throws IOException {
        //Given
        IndexSearcher mockedSearcher = mock(IndexSearcher.class);
        searchService.searcher = mockedSearcher;
        //When
        doThrow(IOException.class).when(mockedSearcher).search(any(),anyInt());
        //Then
        assertThrows(FhirResourceSearcherException.class, () -> searchService.getMatchingResourcesAsString());

    }
}
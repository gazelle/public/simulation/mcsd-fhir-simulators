package net.ihe.gazelle.simulator.server.technical.indexers;

import com.fasterxml.jackson.core.JsonProcessingException;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.AlreadyClosedException;
import org.hl7.fhir.r4.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.spy;

class FhirResourceIndexerServiceTest {

    String resourceName = "practitioner";
    FhirResourceIndexerService service;

    @BeforeEach
    void init(){
        service = spy(new FhirResourceIndexerService(resourceName));
    }


    @Test
    void configureIndexer() {
        //When
        service.configureIndexer();
        //Then
        assertNotNull(service.getAnalyzer());
        assertNotNull(service.getDirectory());
        assertNotNull(service.indexWriter);
        assertNotNull(service.config);
    }

    @Test
    void addIndex() {
        //When
        service.addIndex(new Document(), "_index", "test");
        //Then
        assertEquals(resourceName+"_index",service.getUsedIndexes().get("_index"));
    }

    @Test
    void addIgnoreCaseIndex() {
        //When
        service.addIgnoreCaseIndex(new Document(), "_test", "TEST");
        //Then
        assertEquals(resourceName+"_test_ic",service.getUsedIndexes().get("_test_ic"));
    }

    @Test
    void addIdentifier() {
        List<Identifier> identifiers = new ArrayList<>();
        Identifier identifier = new Identifier();
        identifier.setSystem("http://test");
        identifier.setValue("123");
        identifiers.add(identifier);
        service.addIdentifier(new Document(), identifiers);
        assertEquals((resourceName+FhirResourceIndexerService.IDENTIFIER),service.getUsedIndexes().get(FhirResourceIndexerService.IDENTIFIER));
        assertEquals((resourceName+FhirResourceIndexerService.IDENTIFIER_SYSTEM),service.getUsedIndexes().get(FhirResourceIndexerService.IDENTIFIER_SYSTEM));
        assertEquals((resourceName+FhirResourceIndexerService.IDENTIFIER_VALUE),service.getUsedIndexes().get(FhirResourceIndexerService.IDENTIFIER_VALUE));
    }

    @Test
    void addReference() {
        //Given
        Reference reference = new Reference();
        reference.setReference("test");
        //When
        service.addReference(new Document(), "_index", reference);
        //Then
        assertEquals(resourceName+"_index",service.getUsedIndexes().get("_index"));
    }

    @Test
    void addName() {
        //Given
        List<HumanName> names = new ArrayList<HumanName>();
        HumanName name = new HumanName();
        name.setFamily("Simpson");
        name.addGiven("Homer");
        name.addGiven("Jay");
        names.add(name);
        //When
        service.addHumanName(new Document(), names);
        //Then
        assertEquals(resourceName+FhirResourceIndexerService.FAMILY_NAME,service.getUsedIndexes().get(FhirResourceIndexerService.FAMILY_NAME));
        assertEquals(resourceName+FhirResourceIndexerService.GIVEN_NAME,service.getUsedIndexes().get(FhirResourceIndexerService.GIVEN_NAME));

    }

    @Test
    void addType() {
        //given
        List<CodeableConcept> types = new ArrayList<>();
        CodeableConcept codeableConcept = new CodeableConcept();
        Coding coding = new Coding();

        coding.setSystem("http://test");
        coding.setCode("test");
        codeableConcept.addCoding(coding);
        types.add(codeableConcept);
        //When
        service.addType(new Document(), types);
        //Then
        assertEquals((resourceName+FhirResourceIndexerService.TYPE),service.getUsedIndexes().get(FhirResourceIndexerService.TYPE));
        assertEquals((resourceName+FhirResourceIndexerService.TYPE+FhirResourceIndexerService.SYSTEM),service.getUsedIndexes().get("_type_system"));
        assertEquals((resourceName+FhirResourceIndexerService.TYPE+FhirResourceIndexerService.CODE),service.getUsedIndexes().get("_type_code"));
    }

    @Test
    void addDate() {
        //When
        service.addDate(new Document(), "_date", new Date());
        //Then
        assertEquals((resourceName+"_date"),service.getUsedIndexes().get("_date"));
    }

    @Test
    void addStatus() {
        //When
        service.addStatus(new Document(), "true");
        //Then
        assertEquals((resourceName+"_status"),service.getUsedIndexes().get("_status"));
    }
    @Test
    void closeIndexer() throws IOException {
        //When
        service.closeIndexer();
        //Then
        assertThrows(AlreadyClosedException.class, () ->service.indexWriter.commit());
    }


    @Test
    void addDocument() throws IOException {
        //When
        service.addDocument(new Document());
        service.indexWriter.close();
        //Then
        assertNotNull(service.indexWriter.getDirectory());
    }

    @Test
    void addResource() {
        String stringResource = new FhirResourceParser().serialize(new Practitioner(), FhirResourceParser.JSON);
        //When
        service.addResource(new Document(), stringResource);
        //Then
        assertEquals((resourceName+"_resource"),service.getUsedIndexes().get("_resource"));
    }


    @Test
    void getIndexSearcher() throws IOException {
        //Given
        Document doc = new Document();
        service.addIndex(doc, "_index", "test");
        service.addDocument(doc);
        service.closeIndexer();
        //When
        IndexSearcher searcher = service.getIndexSearcher();
        //Then
        assertNotNull(searcher);
    }



}
package net.ihe.gazelle.simulator.server.technical.rest;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class CHITI90RestfulServerTest {

    @Test
    void testInitialize() {
        CHITI90RestfulServer server = new CHITI90RestfulServer();
        assertDoesNotThrow(server::initialize);
    }
}
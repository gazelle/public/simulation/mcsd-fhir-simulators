package net.ihe.gazelle.simulator.server.technical.rest;

import net.ihe.gazelle.simulator.server.technical.factory.providers.MCSDResourceProvidersFactoryProviderImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class IHEITI90RestfulServerTest {

    @Test
    void testInitialize() {
        IHEITI90RestfulServer server = new IHEITI90RestfulServer();
        assertDoesNotThrow(server::initialize);
    }

    @Test
    void getProvidersFactories() {
        assertNotNull(IHEITI90RestfulServer.getProvidersFactories(MCSDResourceProvidersFactoryProviderImpl.IHE_PROFILE));
    }

    @Test
    void initiateListOfProvidersForServlet() {
        assertNotNull(new IHEITI90RestfulServer().initiateListOfProvidersForServlet(IHEITI90RestfulServer.getProvidersFactories(MCSDResourceProvidersFactoryProviderImpl.IHE_PROFILE)));
    }
}
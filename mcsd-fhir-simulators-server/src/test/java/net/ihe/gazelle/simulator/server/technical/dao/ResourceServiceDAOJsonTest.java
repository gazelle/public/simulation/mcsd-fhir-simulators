package net.ihe.gazelle.simulator.server.technical.dao;

import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import net.ihe.gazelle.simulator.server.technical.indexers.FhirResourceSearchProcessorService;
import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerRoleResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Endpoint;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ResourceServiceDAOJsonTest {

    ResourceServiceDAOJson resourceServiceDAOJson;
    List<String> list;
    FhirResourceParser parser = new FhirResourceParser();
    List<Resource> listR;
    Map<String, IBaseResource> map;

    @BeforeEach
     void setUp() {
        resourceServiceDAOJson = new ResourceServiceDAOJson("Practitioner", new PractitionerRoleResourceIndexer());
        String practitionerS1 = """
                {"resourceType":"Practitioner","id":"123"}
                """;
        String practitionerS2 = """
                {"resourceType":"Practitioner","id":"456"}
                """;
        String endpointS1 = """
                {"resourceType":"Endpoint","id":"456"}
                """;
        list = Arrays.asList(practitionerS1,practitionerS2);
        Resource practitioner1 = (Resource) parser.deserialize(practitionerS1, Practitioner.class);
        Resource practitioner2 = (Resource) parser.deserialize(practitionerS2, Practitioner.class);
        Resource endpoint1 = (Resource) parser.deserialize(endpointS1, Endpoint.class);
        listR = Arrays.asList(practitioner1, practitioner2, endpoint1);

        map = Map.of(practitioner1.getId(), practitioner1,
                practitioner2.getId(),practitioner2,
                endpoint1.getId(), endpoint1);
    }

    @Test
    void setupIndexerAndSearcher() {
        assertDoesNotThrow(() ->new ResourceServiceDAOJson("Practitioner", new PractitionerRoleResourceIndexer()));
    }

    @Test
    void getAllResourcesByResourceType() {
        resourceServiceDAOJson.cache = map;
        List<IBaseResource> listAll = resourceServiceDAOJson.getAllResourcesByResourceType("Practitioner");
        assertNotEquals(0, listAll.size());
        for (IBaseResource resource : listAll){
            assertInstanceOf(Practitioner.class, resource);
        }
    }

    @Test
    void getIndexes() {
        assertNotNull(resourceServiceDAOJson.getIndexes());
    }

    @Test
    void queryResourceByExactString() {
        assertDoesNotThrow(() -> resourceServiceDAOJson.queryResourceByExactString("practitioner_id", "toto"));

    }

    @Test
    void getResourcesFromSearch() throws FhirResourceSearcherException {
        FhirResourceSearchProcessorService searcherMock = Mockito.mock(FhirResourceSearchProcessorService.class);
        Mockito.doReturn(list).when(searcherMock).getMatchingResourcesAsString();
        resourceServiceDAOJson.searcher = searcherMock;
        when(searcherMock.getMatchingResourcesAsString()).thenReturn(list);
        assertEquals(2, resourceServiceDAOJson.getResourcesFromSearch("Practitioner").size());
    }

    @Test
    void getResourceByIdNull() {
        assertNull(resourceServiceDAOJson.getResourceWithId(null));
    }

    @Test
    void getResourceById() {
        resourceServiceDAOJson.cache = map;
        String idToFind = "Practitioner/123";
        assertInstanceOf(Practitioner.class, resourceServiceDAOJson.getResourceWithId(idToFind));
        Practitioner practitioner = (Practitioner) resourceServiceDAOJson.getResourceWithId(idToFind);
        assertEquals(idToFind,practitioner.getId());
    }

    @Test
    void getResourceByIdBlank() {
        resourceServiceDAOJson.cache = map;
        String idToFind = "";
        assertNull(resourceServiceDAOJson.getResourceWithId(idToFind));
    }
}
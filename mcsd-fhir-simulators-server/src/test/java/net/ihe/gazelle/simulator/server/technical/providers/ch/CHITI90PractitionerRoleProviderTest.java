package net.ihe.gazelle.simulator.server.technical.providers.ch;

import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CHITI90PractitionerRoleProviderTest {

    @Mock
    private FhirResourceService service;

    @InjectMocks
    CHITI90PractitionerRoleProvider provider = new CHITI90PractitionerRoleProvider();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetResourceType() {
        assertEquals(PractitionerRole.class, provider.getResourceType());
    }

    @Test
    void testOKRead() {
            //Given
            IdType idType = new IdType(1);
            //When
            when(service.getResourceById(any())).thenReturn(new PractitionerRole().setId("1"));
            IBaseResource resource = provider.read(idType);
            //Then
            assertEquals("1", resource.getIdElement().getIdPart());
    }

    @Test
    void readKO() {
            //Given
            IdType idType = new IdType(1);
            //When
            when(service.getResourceById(any())).thenReturn(null);
            //Then
            Exception exception = assertThrows(ResourceNotFoundException.class, () -> provider.read(idType));
            assertEquals(new ResourceNotFoundException(idType).getMessage(), exception.getMessage());
    }

    @Test
    void searchPractitionerRoleWithParams() throws FhirResourceServiceException {
            //When
            when(service.getResourcesFromParams(any())).thenReturn(new ArrayList<>());
            List<IBaseResource> list = provider.searchPractitionerRole(new TokenParam(),new DateRangeParam(),  new TokenParam(),new ReferenceParam(), new ReferenceParam(), new TokenParam(),new TokenParam(), new HashSet<>());
            //Then
            assertNotNull(list);
    }

    @Test
    void searchPractitionerRoleWithNull() throws FhirResourceServiceException {
            //Given
            //When
            List<IBaseResource> list = provider.searchPractitionerRole(null,null,  null,null,null,null,null, null);
            //Then
            assertNotNull(list);
    }
}
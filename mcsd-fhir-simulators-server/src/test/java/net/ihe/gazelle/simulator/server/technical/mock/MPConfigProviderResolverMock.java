package net.ihe.gazelle.simulator.server.technical.mock;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.spi.ConfigBuilder;
import org.eclipse.microprofile.config.spi.ConfigProviderResolver;

public class MPConfigProviderResolverMock extends ConfigProviderResolver {

    @Override
    public Config getConfig() {
        return new MPConfigMock();
    }

    @Override
    public Config getConfig(ClassLoader classLoader) {
        return null;
    }

    @Override
    public ConfigBuilder getBuilder() {
        return null;
    }

    @Override
    public void registerConfig(Config config, ClassLoader classLoader) {

    }

    @Override
    public void releaseConfig(Config config) {

    }
}

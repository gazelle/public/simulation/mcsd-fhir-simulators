package net.ihe.gazelle.simulator.server.technical.io;

import net.ihe.gazelle.simulator.server.technical.indexers.EndpointResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.indexers.FhirResourceIndexerService;
import net.ihe.gazelle.simulator.server.technical.indexers.FhirResourceSearchProcessorService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Endpoint;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class SearchEngineLuceneTest {

    static String resourceEndpoint = """
            {
              "resourceType": "Endpoint",
              "identifier": [
                {
                  "system": "http://example.com/endpoints",
                  "value": "12345"
                }
              ],
              "managingOrganization": {
                "reference": "Organization/ABCHealthcare"
              },
              "status": "active"
            }
            """;

    static String resourceEndpoint2 = """
            {
              "resourceType": "Endpoint",
              "identifier": [
                {
                  "system": "http://example.com/endpoints",
                  "value": "82348"
                }
              ],
              "managingOrganization": {
                "reference": "Organization/ABCHealthcare"
              },
              "status": "active"
            }
            """;

    List<String> listEndpoint = new ArrayList<>();
    FhirResourceParser parser = new FhirResourceParser();
    EndpointResourceIndexer indexer;
    FhirResourceSearchProcessorService searchProcessorService;


    void prepareList() {
        listEndpoint.add(resourceEndpoint);
        listEndpoint.add(resourceEndpoint2);
    }

    void prepareIndexationOfResource() throws IOException {
        indexer = new EndpointResourceIndexer();
        indexer.createIndexes(listEndpoint);
        searchProcessorService = new FhirResourceSearchProcessorService(indexer);
    }


    @Test
    void testSearchLuceneExact() throws Exception {
        //GIVEN
        prepareList();
        prepareIndexationOfResource();

        String fieldName = FhirResourceIndexerService.IDENTIFIER;
        String queryToFind = "http://example.com/endpoints|12345";

        //WHEN
        searchProcessorService.exactSearch(fieldName, queryToFind);
        List<String> results = searchProcessorService.getMatchingResourcesAsString();

        //THEN
        if (1 == results.size()) {
            Endpoint r = (Endpoint) parser.deserialize(results.get(0), Endpoint.class);
            assertEquals("12345", r.getIdentifier().get(0).getValue());
        } else {
            fail("Should return only one hit, actual: " + results.size());
        }
    }


    @Test
    void testSearchLuceneExact2() throws Exception {
        //GIVEN
        prepareList();
        prepareIndexationOfResource();
        String fieldName = FhirResourceIndexerService.ORGANIZATION_REFERENCE;
        String queryToFind = "Organization/ABCHealthcare";

        //WHEN
        searchProcessorService.exactSearch(fieldName, queryToFind);
        List<String> results = searchProcessorService.getMatchingResourcesAsString();

        //THEN
        if (2 == results.size()) {
            Endpoint r = (Endpoint) parser.deserialize(results.get(0), Endpoint.class);
            assertEquals("12345", r.getIdentifier().get(0).getValue());
            Endpoint r2 = (Endpoint) parser.deserialize(results.get(1), Endpoint.class);
            assertEquals("82348", r2.getIdentifier().get(0).getValue());

        } else {
            fail("Should return only 2 hits, actual: " + results.size());
        }

    }


}
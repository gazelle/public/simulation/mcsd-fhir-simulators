package net.ihe.gazelle.simulator.server.technical.services;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.mock.FhirResourceDAOJsonMock;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Reference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FhirResourceServiceTest {

    Map<String, BaseParam> params;
    private ResourceServiceDAO dao = new FhirResourceDAOJsonMock();
    FhirResourceService service;


    @BeforeEach
    void setUp(){
        service = new FhirResourceService(dao,"Practitioner");
        params = new HashMap<>();
        DateRangeParam theLastUpdated = new DateRangeParam();
        params.put("_active", new TokenParam().setValue("true"));
        params.put("_lastUpdated_lb", theLastUpdated.getLowerBound());
        params.put("_lastUpdated_ub", theLastUpdated.getUpperBound());
        params.put("_test_ref", new ReferenceParam());
        params.put("_test_string", new StringParam().setValue("test"));
    }

    @Test
    void getResourceWithId()  {
        assertEquals("test", service.getResourceById("test").getIdElement().getIdPart());
    }


    @Test
    void processSearchByParams() throws FhirResourceServiceException {
        //when
        List<IBaseResource> resources = service.processSearchByParams(params);

        assertNotNull(resources);
    }

    @Test
    void processSearchByParamsThrow() {
        //when
        service = new FhirResourceService(dao,"exception");
        //then
        assertDoesNotThrow(() -> service.processSearchByParams(params));

    }

    @Test
    void processSearchByStringParamExact(){
        StringParam stringParam = new StringParam("_test");
        stringParam.setExact(true);
        assertDoesNotThrow(() -> service.processSearchByStringParam("_test", stringParam));
    }

    @Test
    void processSearchByStringParamContains() {
        StringParam stringParam = new StringParam("_test");
        stringParam.setContains(true);
        assertDoesNotThrow(() -> service.processSearchByStringParam("_test", stringParam));
    }
    @Test
    void processSearchByStringParam() {
        StringParam stringParam = new StringParam("_test");
        assertDoesNotThrow(() -> service.processSearchByStringParam("_test", stringParam));
    }

    @Test
    void processSearchByTokenParam() {
        TokenParam tokenParam = new TokenParam("_test");
        assertDoesNotThrow(() -> service.processSearchByTokenParam("_test", tokenParam));
    }

    @Test
    void processSearchByTokenParamIdentifierWithAll() {
        TokenParam tokenParam = new TokenParam();
        assertDoesNotThrow(() -> service.processSearchByTokenParam("_identifier", tokenParam));
        tokenParam.setValue("123");
        tokenParam.setSystem("http://localhost");
        assertDoesNotThrow(() -> service.processSearchByTokenParam("_identifier", tokenParam));
    }

    @Test
    void processSearchByTokenParamIdentifierEmpty() {
        TokenParam tokenParam = new TokenParam();
        assertDoesNotThrow(() -> service.processSearchByTokenParam("_identifier", tokenParam));
    }

    @Test
    void processSearchByTokenParamIdentifierSystemOnly() {
        TokenParam tokenParam = new TokenParam();
        tokenParam.setSystem("http://localhost");
        assertDoesNotThrow(() -> service.processSearchByTokenParam("_identifier", tokenParam));
    }

    @Test
    void processSearchByTokenParamIdentifierValueOnly() {
        TokenParam tokenParam = new TokenParam();
        tokenParam.setValue("123");
        assertDoesNotThrow(() -> service.processSearchByTokenParam("_identifier", tokenParam));
    }


    @Test
    void processSearchByReferenceParam() {
        ReferenceParam referenceParam = new ReferenceParam("_test");
        assertDoesNotThrow(() -> service.processSearchByReferenceParam("_test", referenceParam));
    }

    @Test
    void processSearchByDateParamEqual() {
        DateParam dateParam = new DateParam();
        dateParam.setPrefix(ParamPrefixEnum.EQUAL);
        dateParam.setValue(new Date());
        assertDoesNotThrow(() -> service.processSearchByDateParam("_lastUpdated_test", dateParam));
    }
    @Test
    void processSearchByDateParamLess() {
        DateParam dateParamLt = new DateParam();
        dateParamLt.setValue(new Date());
        dateParamLt.setPrefix(ParamPrefixEnum.LESSTHAN);
        assertDoesNotThrow(() -> service.processSearchByDateParam("_date", dateParamLt));
        dateParamLt.setPrefix(ParamPrefixEnum.LESSTHAN_OR_EQUALS);
        assertDoesNotThrow(() -> service.processSearchByDateParam("_date", dateParamLt));
    }

    @Test
    void processSearchByDateParamGreater() {
        DateParam dateParam = new DateParam();
        dateParam.setValue(new Date());
        dateParam.setPrefix(ParamPrefixEnum.GREATERTHAN);
        assertDoesNotThrow(() -> service.processSearchByDateParam("_date", dateParam));
        dateParam.setPrefix(ParamPrefixEnum.GREATERTHAN_OR_EQUALS);
        assertDoesNotThrow(() -> service.processSearchByDateParam("_date", dateParam));
    }
    @Test
    void processSearchByDateParamThrow() {
        DateParam dateParam = new DateParam();
        dateParam.setPrefix(ParamPrefixEnum.APPROXIMATE);
        dateParam.setValue(new Date());
        assertThrows(FhirResourceServiceException.class,() -> service.processSearchByDateParam("_lastUpdated_test", dateParam));
    }

    @Test
    void getResourcesFromParams() throws FhirResourceServiceException {
        List<IBaseResource> resources = service.getResourcesFromParams(params);
        assertEquals(1, resources.size());
    }

    @Test
    void getResourcesFromParamsThrowException() {
        service = new FhirResourceService(dao,"other");
        assertDoesNotThrow(() -> service.getResourcesFromParams(Map.of()));
    }

    @Test
    void setResourcesFromIncludes() {
        PractitionerRole practitionerRole1 = new PractitionerRole();
        practitionerRole1.setPractitioner(new Reference("test"));
        PractitionerRole practitionerRole2 = new PractitionerRole();
        assertDoesNotThrow(() -> service.setResourcesFromIncludes(List.of(practitionerRole1, practitionerRole2), Set.of(new Include("PractitionerRole:practitioner"), new Include("PractitionerRole:test"))));
        assertDoesNotThrow(() -> service.setResourcesFromIncludes(List.of(practitionerRole1, practitionerRole2), Set.of(new Include("PractitionerRole:test"))));

    }
}
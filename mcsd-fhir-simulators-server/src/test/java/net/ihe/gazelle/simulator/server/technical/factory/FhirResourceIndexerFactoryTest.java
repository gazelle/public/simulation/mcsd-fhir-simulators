package net.ihe.gazelle.simulator.server.technical.factory;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerResourceIndexer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNull;

class FhirResourceIndexerFactoryTest {

    @Test
    void createPractitionerIndexer() {
        FhirResourceIndexerFactory factory = new FhirResourceIndexerFactory();
        FhirResourceIndexer indexer = factory.createIndexer("Practitioner");
        assertInstanceOf(PractitionerResourceIndexer.class, indexer);
    }

    @Test
    void createIndexerNotSupportedResource() {
        FhirResourceIndexerFactory factory = new FhirResourceIndexerFactory();
        assertNull(factory.createIndexer("Patient"));
    }
}
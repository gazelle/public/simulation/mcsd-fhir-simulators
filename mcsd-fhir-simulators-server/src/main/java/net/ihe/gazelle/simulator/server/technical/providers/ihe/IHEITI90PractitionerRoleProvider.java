package net.ihe.gazelle.simulator.server.technical.providers.ihe;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.param.BaseParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.business.ResourceService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.dao.ResourceServiceDAOJson;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerRoleResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IAnyResource;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.PractitionerRole;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IHEITI90PractitionerRoleProvider implements IResourceProvider {

    ResourceService resourceService;
    ResourceServiceDAO dao;
    private static final String RESOURCE_TYPE = "PractitionerRole";

    public IHEITI90PractitionerRoleProvider(PractitionerRoleResourceIndexer practitionerRoleResourceIndexer) {
        this.dao = new ResourceServiceDAOJson(RESOURCE_TYPE, practitionerRoleResourceIndexer);
        this.resourceService = new FhirResourceService(dao, RESOURCE_TYPE);
    }

    public IHEITI90PractitionerRoleProvider() {
        this(new PractitionerRoleResourceIndexer());
    }



    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return PractitionerRole.class;
    }

    @Read
    public IBaseResource read(@IdParam IdType theId) {
        IBaseResource resource = resourceService.getResourceById(theId.toString());
        if (resource == null) {
            throw new ResourceNotFoundException(theId);
        }
        return resource;
    }

    @Search
    public List<IBaseResource> searchPractitionerRole(@OptionalParam(name = IAnyResource.SP_RES_ID) TokenParam theID,
                                                      @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
                                                      @OptionalParam(name = PractitionerRole.SP_ACTIVE) TokenParam activeStatus,
                                                      @OptionalParam(name = PractitionerRole.SP_ORGANIZATION) ReferenceParam theOrganization,
                                                      @OptionalParam(name = PractitionerRole.SP_PRACTITIONER) ReferenceParam thePractitioner,
                                                      @OptionalParam(name = PractitionerRole.SP_ROLE) TokenParam theRole,
                                                      @OptionalParam(name = PractitionerRole.SP_SPECIALTY) TokenParam theSpecialty,
                                                      @IncludeParam(allow = {"PractitionerRole:practitioner"}) Set<Include> theIncludes) throws FhirResourceServiceException {

        Map<String, BaseParam> params = new HashMap<>();
        if (theID != null) params.put("_id", theID);
        if (theLastUpdated != null) {
            params.put("_lastUpdated_lb", theLastUpdated.getLowerBound());
            params.put("_lastUpdated_ub", theLastUpdated.getUpperBound());
        }
        if (theOrganization != null) params.put("_organization_reference", theOrganization);
        if (thePractitioner != null) params.put("_practitioner_reference", thePractitioner);
        if (theRole != null) params.put("_role", theRole);
        if (theSpecialty != null) params.put("_specialty", theSpecialty);
        if (activeStatus != null) params.put("_active", activeStatus);


        List<IBaseResource> resources = resourceService.getResourcesFromParams(params);
        if (theIncludes != null) resourceService.setResourcesFromIncludes(resources, theIncludes);

        return resources;
    }
}

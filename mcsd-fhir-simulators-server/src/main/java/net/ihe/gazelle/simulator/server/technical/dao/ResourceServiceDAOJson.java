package net.ihe.gazelle.simulator.server.technical.dao;

import ca.uhn.fhir.context.FhirContext;
import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.business.ResourceLoaderService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import net.ihe.gazelle.simulator.server.technical.indexers.FhirResourceSearchProcessorService;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import net.ihe.gazelle.simulator.server.technical.services.loader.ResourceLoaderServiceJson;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ResourceServiceDAOJson implements ResourceServiceDAO {

    Logger logger = LoggerFactory.getLogger(ResourceServiceDAOJson.class);

    Map<String, IBaseResource> cache;
    FhirResourceSearchProcessorService searcher;

    Map<String, List<String>> stringResources;
    private FhirResourceIndexer indexer;
    private final FhirResourceParser parser = new FhirResourceParser();

    private final ResourceLoaderService resourceLoaderServiceJson = new ResourceLoaderServiceJson();


    public ResourceServiceDAOJson(String resource, FhirResourceIndexer indexer) {
        this.indexer = indexer;
        this.stringResources = resourceLoaderServiceJson.getStringResources();
        this.cache = initiateMapFromResources();
        setupIndexerAndSearcher(resource);
    }

    void setupIndexerAndSearcher(String resource) {
        try {
            if(stringResources.containsKey(resource)) {
                indexer.createIndexes(stringResources.get(resource));
            }
            this.searcher = new FhirResourceSearchProcessorService(indexer);
        } catch (NullPointerException | IOException e) {
            logger.error("Problem during setup of indexer and searcher for resource: {}", resource, e);
        }

    }

    public IBaseResource getResourceWithId(String id) {
        IBaseResource resource = null;
        if (id != null && !id.isBlank()) {
            resource = cache.get(id);
        }
        return resource;
    }

    public List<IBaseResource> getAllResourcesByResourceType(String resourceType) {
        return cache.values().stream()
                .filter(resource -> resource.fhirType().equals(resourceType))
                .toList();

    }

    @Override
    public Map<String, String> getIndexes() {
        return indexer.getMapOfIndexes();
    }

    Map<String, IBaseResource> initiateMapFromResources() {
        return resourceLoaderServiceJson.getResources().stream()
                .map(Resource.class::cast)
                .filter(resource -> resource.getId() != null)
                .collect(Collectors.toMap(Resource::getId, Function.identity()));
    }

    public void queryResourceByExactString(String index, String name) {
        searcher.exactSearch(index, name);
    }

    public void queryResourceByApproximateString(String index, String stringToSearch) {
        searcher.approximateSearch(index, stringToSearch);
    }

    public void queryResourceByPrefix(String index, String stringToSearch) {
        searcher.prefixSearch(index, stringToSearch);
    }

    public void queryResourceByDate(String index, long queryMin, long queryMax) {
        searcher.longNumberSearch(index, queryMin, queryMax);
    }

    public List<IBaseResource> getResourcesFromSearch(String resourceName) throws FhirResourceSearcherException {
        List<IBaseResource> returnedResources = new ArrayList<>();
        List<String> results = searcher.getMatchingResourcesAsString();
        Class<? extends IBaseResource> resourceClass = FhirContext.forR4().getResourceDefinition(resourceName).getImplementingClass();
        for (String resource : results) {
            returnedResources.add(parser.deserialize(resource, resourceClass));
        }
        return returnedResources;

    }

}

package net.ihe.gazelle.simulator.server.technical.providers.ch;

import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParserCH;
import net.ihe.gazelle.simulator.server.technical.providers.ihe.IHEITI90PractitionerProvider;

public class CHITI90PractitionerProvider extends IHEITI90PractitionerProvider {

    public CHITI90PractitionerProvider() {
        super(new PractitionerResourceIndexer(new FhirResourceParserCH()));
    }
}

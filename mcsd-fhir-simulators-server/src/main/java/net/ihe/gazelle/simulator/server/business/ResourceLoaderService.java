package net.ihe.gazelle.simulator.server.business;

import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.List;
import java.util.Map;

public interface ResourceLoaderService {

    List<IBaseResource> getResources();

    Map<String, List<String>> getStringResources();

}

package net.ihe.gazelle.simulator.server.technical.services.loader;

import ca.uhn.fhir.context.FhirContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import net.ihe.gazelle.simulator.server.business.ResourceLoaderService;
import net.ihe.gazelle.simulator.server.technical.exception.ResourceLoaderServiceJsonException;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class ResourceLoaderServiceJson implements ResourceLoaderService {

    private static final List<IBaseResource> resources = new ArrayList<>();

    private static final Map<String, List<String>> stringResources = new HashMap<>();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final FhirResourceParser mapping = new FhirResourceParser();
    private final FhirContext ctx = FhirContext.forR4();


    @ConfigProperty(name = "resources.summary.path")
    String jsonPath;

    void onStart(@Observes StartupEvent ev) throws ResourceLoaderServiceJsonException {
        try {
            JsonNode entriesNodesFromSummary = convertSummaryFileAsJsonNodes();

            for (JsonNode entryNode : entriesNodesFromSummary) {

                String resourceType = entryNode.get("resourceType").asText();
                String resourcesDirectory = entryNode.get("resourcesDirectory").asText();
                Class<? extends IBaseResource> classOfResourceType = ctx.getResourceDefinition(resourceType).getImplementingClass();

                addSpecificResourcesIntoList(getAbsolutePathsListOfFhirResources(resourcesDirectory), classOfResourceType);
            }
        } catch (IOException e) {
            throw new ResourceLoaderServiceJsonException("Cannot not parse JSON from summary:" + e.getCause());
        }

    }

    JsonNode convertSummaryFileAsJsonNodes() throws IOException {
        Path summaryPath = Paths.get(jsonPath);
        String summaryAsString = Files.readString(summaryPath, StandardCharsets.UTF_8);
        return objectMapper.readValue(summaryAsString, JsonNode.class);
    }

    private void addSpecificResourcesIntoList(Set<String> resourcesForOneType, Class<? extends IBaseResource> resourceClass) throws IOException {
        for (String file : resourcesForOneType) {
            String resourceAsString = Files.readString(Path.of(file), StandardCharsets.UTF_8);
            IBaseResource resource = mapping.deserialize(resourceAsString, resourceClass);
            resources.add(resource);
            if(stringResources.containsKey(resourceClass.getSimpleName())){
                stringResources.get(resourceClass.getSimpleName()).add(resourceAsString);
            }else{
                stringResources.put(resourceClass.getSimpleName(), new ArrayList<>(List.of(resourceAsString)));
            }
        }
    }

    public Set<String> getAbsolutePathsListOfFhirResources(String dir) {
        return Stream.of(Objects.requireNonNull(new File(dir).listFiles()))
                .filter(file -> !file.isDirectory())
                .map(File::getAbsolutePath)
                .collect(Collectors.toSet());
    }

    @Override
    public List<IBaseResource> getResources() {
        return resources;
    }

    @Override
    public Map<String, List<String>> getStringResources() {
        return stringResources;
    }
}


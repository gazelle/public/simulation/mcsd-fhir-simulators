package net.ihe.gazelle.simulator.server.technical.factory;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.indexers.EndpointResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.indexers.OrganizationResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerRoleResourceIndexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class FhirResourceIndexerFactory {

    Logger log = LoggerFactory.getLogger(FhirResourceIndexerFactory.class);
    static Map<String, FhirResourceIndexer> mapOfIndexer = Map.of(
            "Practitioner", new PractitionerResourceIndexer(),
            "PractitionerRole", new PractitionerRoleResourceIndexer(),
            "Endpoint", new EndpointResourceIndexer(),
            "Organization", new OrganizationResourceIndexer()
    );

    public FhirResourceIndexer createIndexer(String resource) {
        FhirResourceIndexer indexer = null;
        if (mapOfIndexer.containsKey(resource)) {
            indexer = mapOfIndexer.get(resource);
        } else {
            log.error("Unsupported FHIR type: {}", resource);
        }
        return indexer;

    }
}

package net.ihe.gazelle.simulator.server.technical.exception;

public class ResourceLoaderServiceJsonException extends Exception {

    public ResourceLoaderServiceJsonException(String message) {
        super(message);
    }

}

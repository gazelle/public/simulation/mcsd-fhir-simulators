package net.ihe.gazelle.simulator.server.technical.exception;

public class FhirResourceServiceException extends Exception {

    public FhirResourceServiceException(String message) {
        super(message);
    }
}

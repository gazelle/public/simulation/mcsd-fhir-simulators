package net.ihe.gazelle.simulator.server.technical.providers.ch;

import net.ihe.gazelle.simulator.server.technical.indexers.OrganizationResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParserCH;
import net.ihe.gazelle.simulator.server.technical.providers.ihe.IHEITI90OrganizationProvider;

public class CHITI90OrganizationProvider extends IHEITI90OrganizationProvider {


    public CHITI90OrganizationProvider() {
        super(new OrganizationResourceIndexer(new FhirResourceParserCH()));
    }
}

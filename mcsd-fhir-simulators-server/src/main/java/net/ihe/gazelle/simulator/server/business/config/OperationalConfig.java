package net.ihe.gazelle.simulator.server.business.config;

public interface OperationalConfig {

    String getEvsEndpoint();
    String getServerBaseUrl(String profile);

}

package net.ihe.gazelle.simulator.server.technical.rest;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import jakarta.enterprise.inject.Alternative;
import net.ihe.gazelle.simulator.server.business.MCSDResourcesProvidersFactory;
import net.ihe.gazelle.simulator.server.technical.factory.providers.MCSDResourceProvidersFactoryProviderImpl;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

@Alternative
public class IHEITI90RestfulServer extends RestfulServer {

    @Serial
    private static final long serialVersionUID = 1L;

    public IHEITI90RestfulServer() {
        super(FhirContext.forR4Cached());
    }

    @Override
    protected void initialize() {
        List<MCSDResourcesProvidersFactory> providerFactories = getProvidersFactories(MCSDResourceProvidersFactoryProviderImpl.IHE_PROFILE);
        setResourceProviders(initiateListOfProvidersForServlet(providerFactories));
    }

    protected static List<MCSDResourcesProvidersFactory> getProvidersFactories(String profile) {
        return new MCSDResourceProvidersFactoryProviderImpl().createListOfMCSDResourceProviders(profile);
    }

    protected List<IResourceProvider> initiateListOfProvidersForServlet(List<MCSDResourcesProvidersFactory> providerFactories) {
        List<IResourceProvider> providers = new ArrayList<>();
        for (MCSDResourcesProvidersFactory factory : providerFactories) {
            providers.add(factory.createMCSDResourceProviders());
        }
        return providers;
    }

}

package net.ihe.gazelle.simulator.server.technical.providers.ihe;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.BaseParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.business.ResourceService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.dao.ResourceServiceDAOJson;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.indexers.EndpointResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IAnyResource;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Endpoint;
import org.hl7.fhir.r4.model.IdType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IHEITI90EndpointProvider implements IResourceProvider {

    ResourceService resourceService;
    ResourceServiceDAO dao;
    private static final String RESOURCE_TYPE = "Endpoint";

    public IHEITI90EndpointProvider() {
        this.dao = new ResourceServiceDAOJson(RESOURCE_TYPE, new EndpointResourceIndexer());
        this.resourceService = new FhirResourceService(dao, RESOURCE_TYPE);
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Endpoint.class;
    }

    @Read
    public IBaseResource read(@IdParam IdType theId) {
        IBaseResource resource = resourceService.getResourceById(theId.toString());
        if (resource == null) {
            throw new ResourceNotFoundException(theId);
        }
        return resource;
    }

    @Search
    public List<IBaseResource> search(@OptionalParam(name = IAnyResource.SP_RES_ID) StringParam theID,
                                      @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
                                      @OptionalParam(name = Endpoint.SP_IDENTIFIER) TokenParam theIdentifier,
                                      @OptionalParam(name = Endpoint.SP_ORGANIZATION) StringParam theOrganization,
                                      @OptionalParam(name = Endpoint.SP_STATUS) StringParam theStatus
    ) throws FhirResourceServiceException {


        Map<String, BaseParam> params = new HashMap<>();
        if (theID != null) params.put("_id", theID);
        if (theOrganization != null) params.put("_organization", theOrganization);
        if (theLastUpdated != null) {
            params.put("_lastUpdated_lb", theLastUpdated.getLowerBound());
            params.put("_lastUpdated_ub", theLastUpdated.getUpperBound());
        }
        if (theStatus != null) params.put("_active", theStatus);
        if (theIdentifier != null) params.put("_identifier", theIdentifier);

        return resourceService.getResourcesFromParams(params);
    }


}

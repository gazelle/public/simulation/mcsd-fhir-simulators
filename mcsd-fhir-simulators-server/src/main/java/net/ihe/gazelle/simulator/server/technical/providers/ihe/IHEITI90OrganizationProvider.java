package net.ihe.gazelle.simulator.server.technical.providers.ihe;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.business.ResourceService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.dao.ResourceServiceDAOJson;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.indexers.OrganizationResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IAnyResource;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Organization;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IHEITI90OrganizationProvider implements IResourceProvider {

    ResourceService resourceService;
    ResourceServiceDAO dao;
    private static final String RESOURCE_TYPE = "Organization";


    public IHEITI90OrganizationProvider(OrganizationResourceIndexer organizationResourceIndexer) {
        this.dao = new ResourceServiceDAOJson(RESOURCE_TYPE, organizationResourceIndexer);
        this.resourceService = new FhirResourceService(dao, RESOURCE_TYPE);
    }

    public IHEITI90OrganizationProvider() {
        this(new OrganizationResourceIndexer());
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Organization.class;
    }



    @Read
    public IBaseResource read(@IdParam IdType theId) {
        IBaseResource resource = resourceService.getResourceById(theId.toString());
        if (resource == null) {
            throw new ResourceNotFoundException(theId);
        }
        return resource;
    }


    @Search
    public List<IBaseResource> searchOrganization(@OptionalParam(name = IAnyResource.SP_RES_ID) TokenParam theID,
                                                  @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
                                                  @OptionalParam(name = Organization.SP_NAME) StringParam theName,
                                                  @OptionalParam(name = Organization.SP_ACTIVE) TokenParam activeStatus,
                                                  @OptionalParam(name = Organization.SP_IDENTIFIER) TokenParam theIdentifier,
                                                  @OptionalParam(name = Organization.SP_PARTOF) ReferenceParam thePartOf,
                                                  @OptionalParam(name = Organization.SP_TYPE) TokenParam type) throws FhirResourceServiceException {


        Map<String, BaseParam> params = new HashMap<>();
        if (theID != null) params.put("_id", theID);
        if (theName != null) params.put("_name", theName);
        if (theLastUpdated != null) {
            params.put("_lastUpdated_lb", theLastUpdated.getLowerBound());
            params.put("_lastUpdated_ub", theLastUpdated.getUpperBound());
        }
        if (activeStatus != null) params.put("_active", activeStatus);
        if (theIdentifier != null) params.put("_identifier", theIdentifier);
        if (thePartOf != null) params.put("_partof_organization_reference", thePartOf);
        if (type != null) params.put("_type", type);
        return resourceService.getResourcesFromParams(params);

    }
}

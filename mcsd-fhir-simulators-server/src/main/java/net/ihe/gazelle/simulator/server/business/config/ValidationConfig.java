package net.ihe.gazelle.simulator.server.business.config;

public interface ValidationConfig {

    String getHttpIti90SearchPractitioner();

    String getHttpIti90SearchPractitionerRole();

    String getHttpIti90SearchOrganization();

    String getHttpIti90Read();

    String getHttpValidatorName();

    boolean isValidationEnabled();
}

package net.ihe.gazelle.simulator.server.technical.indexers;


import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.hl7.fhir.r4.model.PractitionerRole;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class PractitionerRoleResourceIndexer implements FhirResourceIndexer {


    public static final String RESOURCE_NAME = "practitionerRole";
    private FhirResourceIndexerService indexerService;

    private final FhirResourceParser parser;


    public PractitionerRoleResourceIndexer(FhirResourceParser parser) {
        this.parser = parser;
        indexerService = new FhirResourceIndexerService(RESOURCE_NAME);
    }

    public PractitionerRoleResourceIndexer() {
        this(new FhirResourceParser());
    }

    public void createIndexes(List<String> stringResources) throws IOException {
        for (String stringResource : stringResources) {
            Document doc = new Document();
            indexerService.addResource(doc, stringResource);
            PractitionerRole practitionerRole = this.parser.deserialize(stringResource, PractitionerRole.class);
            indexerService.addIndex(doc, FhirResourceIndexerService.ID, practitionerRole.getIdPart());
            indexerService.addIndex(doc, FhirResourceIndexerService.IS_ACTIVE, Boolean.toString(practitionerRole.getActive()));
            indexerService.addIdentifier(doc, practitionerRole.getIdentifier());
            indexerService.addReference(doc, FhirResourceIndexerService.ORGANIZATION_REFERENCE, practitionerRole.getOrganization());
            indexerService.addReference(doc, FhirResourceIndexerService.PRACTITIONER_REFERENCE, practitionerRole.getPractitioner());
            indexerService.addRole(doc, practitionerRole.getCode());
            indexerService.addDate(doc, FhirResourceIndexerService.LAST_UPDATED, practitionerRole.getMeta().getLastUpdated());
            indexerService.addSpecialty(doc, practitionerRole.getSpecialty());
            indexerService.addDocument(doc);
        }
        indexerService.closeIndexer();
    }


    @Override
    public Analyzer getAnalyzer() {
        return indexerService.getAnalyzer();
    }

    @Override
    public Directory getDirectory() {
        return indexerService.getDirectory();
    }

    @Override
    public IndexSearcher getSearcher() {
        return indexerService.getIndexSearcher();
    }

    @Override
    public Map<String, String> getMapOfIndexes() {
        return indexerService.getUsedIndexes();
    }


}

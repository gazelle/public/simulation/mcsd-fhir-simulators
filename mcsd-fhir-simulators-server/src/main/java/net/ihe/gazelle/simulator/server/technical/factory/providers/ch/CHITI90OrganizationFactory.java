package net.ihe.gazelle.simulator.server.technical.factory.providers.ch;

import ca.uhn.fhir.rest.server.IResourceProvider;
import net.ihe.gazelle.simulator.server.business.MCSDResourcesProvidersFactory;
import net.ihe.gazelle.simulator.server.technical.factory.providers.MCSDResourceProvidersFactoryProviderImpl;
import net.ihe.gazelle.simulator.server.technical.providers.ch.CHITI90OrganizationProvider;

public class CHITI90OrganizationFactory implements MCSDResourcesProvidersFactory {


    @Override
    public IResourceProvider createMCSDResourceProviders() {
        return new CHITI90OrganizationProvider();
    }

    @Override
    public boolean hasForProfile(String profile) {
        return MCSDResourceProvidersFactoryProviderImpl.CH_PROFILE.equals(profile);
    }


}

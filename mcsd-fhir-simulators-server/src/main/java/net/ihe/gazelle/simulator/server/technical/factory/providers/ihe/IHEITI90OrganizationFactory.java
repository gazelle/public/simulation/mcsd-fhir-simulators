package net.ihe.gazelle.simulator.server.technical.factory.providers.ihe;

import ca.uhn.fhir.rest.server.IResourceProvider;
import net.ihe.gazelle.simulator.server.business.MCSDResourcesProvidersFactory;
import net.ihe.gazelle.simulator.server.technical.factory.providers.MCSDResourceProvidersFactoryProviderImpl;
import net.ihe.gazelle.simulator.server.technical.providers.ihe.IHEITI90OrganizationProvider;

public class IHEITI90OrganizationFactory implements MCSDResourcesProvidersFactory {


    @Override
    public IResourceProvider createMCSDResourceProviders() {
        return new IHEITI90OrganizationProvider();
    }

    @Override
    public boolean hasForProfile(String profile) {
        return MCSDResourceProvidersFactoryProviderImpl.IHE_PROFILE.equals(profile);
    }


}

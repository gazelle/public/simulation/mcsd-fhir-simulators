package net.ihe.gazelle.simulator.server.technical.providers.ihe;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.BaseParam;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.business.ResourceService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.dao.ResourceServiceDAOJson;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IAnyResource;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Practitioner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IHEITI90PractitionerProvider implements IResourceProvider {

    private static final String RESOURCE_TYPE = "Practitioner";
    ResourceService resourceService;
    ResourceServiceDAO dao;


    public IHEITI90PractitionerProvider(PractitionerResourceIndexer practitionerResourceIndexer) {
        this.dao = new ResourceServiceDAOJson(RESOURCE_TYPE, practitionerResourceIndexer);
        this.resourceService = new FhirResourceService(dao, RESOURCE_TYPE);
    }

    public IHEITI90PractitionerProvider() {
        this(new PractitionerResourceIndexer());
    }

    public FhirResourceParser getParser(){
        return new FhirResourceParser();
    }



    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Practitioner.class;
    }

    @Read
    public IBaseResource read(@IdParam IdType theId) {
        IBaseResource resource = resourceService.getResourceById(theId.toString());
        if (resource == null) {
            throw new ResourceNotFoundException(theId);
        }
        return resource;
    }

    @Search
    public List<IBaseResource> searchPractitioner(@OptionalParam(name = IAnyResource.SP_RES_ID) TokenParam theID,
                                                  @OptionalParam(name = "_lastUpdated") DateRangeParam theLastUpdated,
                                                  @OptionalParam(name = Practitioner.SP_ACTIVE) TokenParam activeStatus,
                                                  @OptionalParam(name = Practitioner.SP_IDENTIFIER) TokenParam identifier,
                                                  @OptionalParam(name = Practitioner.SP_NAME) StringParam theName,
                                                  @OptionalParam(name = Practitioner.SP_GIVEN) StringParam theGivenName,
                                                  @OptionalParam(name = Practitioner.SP_FAMILY) StringParam theFamilyName) throws FhirResourceServiceException {

        Map<String, BaseParam> params = new HashMap<>();
        if (theID != null) params.put("_id", theID);
        if (theLastUpdated != null) {
            params.put("_lastUpdated_lb", theLastUpdated.getLowerBound());
            params.put("_lastUpdated_ub", theLastUpdated.getUpperBound());
        }
        if (identifier != null) params.put("_identifier", identifier);
        if (theName != null) params.put("_human_name", theName);
        if (theGivenName != null) params.put("_given_name", theGivenName);
        if (theFamilyName != null) params.put("_family_name", theFamilyName);
        if (activeStatus != null) params.put("_active", activeStatus);

        return resourceService.getResourcesFromParams(params);
    }

}

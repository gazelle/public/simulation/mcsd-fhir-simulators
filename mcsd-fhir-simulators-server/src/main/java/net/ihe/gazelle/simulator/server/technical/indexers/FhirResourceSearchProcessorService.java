package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.index.StoredFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class FhirResourceSearchProcessorService {

    Analyzer analyzer;
    Map<String, String> indexes;
    IndexSearcher searcher;
    BooleanQuery.Builder queryBuilder;

    public FhirResourceSearchProcessorService(FhirResourceIndexer indexer) {
        this.analyzer = indexer.getAnalyzer();
        this.indexes = indexer.getMapOfIndexes();
        this.searcher = indexer.getSearcher();
        this.queryBuilder = new BooleanQuery.Builder();
    }

    public void exactSearch(String fieldName, String queryToFind) {
        Query query = new TermQuery(new Term(indexes.get(fieldName), queryToFind));
        queryBuilder.add(query, BooleanClause.Occur.MUST);
    }

    public void longNumberSearch(String fieldName, long queryMinValue, long queryMaxValue) {
        String field = indexes.get(fieldName);
        if (field != null && !field.isBlank()) {
            Query query = LongPoint.newRangeQuery(field, queryMinValue, queryMaxValue);
            queryBuilder.add(query, BooleanClause.Occur.MUST);
        }

    }

    public void prefixSearch(String fieldName, String givenQuery) {
        String queryToFind = givenQuery == null ? "" : givenQuery;
        Query query = new PrefixQuery(new Term(indexes.get(fieldName + FhirResourceIndexerService.IGNORE_CASE), queryToFind.toLowerCase()));
        queryBuilder.add(query, BooleanClause.Occur.MUST);
    }

    public void approximateSearch(String fieldName, String givenQuery) {
        String queryToFind = givenQuery == null ? "" : givenQuery;
        WildcardQuery query = new WildcardQuery(new Term(indexes.get(fieldName + FhirResourceIndexerService.IGNORE_CASE), "*" + queryToFind.toLowerCase() + "*"));
        queryBuilder.add(query, BooleanClause.Occur.MUST);
    }

    public List<String> getMatchingResourcesAsString() throws FhirResourceSearcherException {
        List<String> results = new ArrayList<>();
        BooleanQuery booleanQuery = queryBuilder.build();
        queryBuilder = new BooleanQuery.Builder();
        try {
            TopDocs hits = searcher.search(booleanQuery, 10);
            StoredFields storedFields = searcher.storedFields();
            for (ScoreDoc hit : hits.scoreDocs) {
                Document doc = storedFields.document(hit.doc);
                results.add(doc.get(indexes.get(FhirResourceIndexerService.RESOURCE)));
            }
        } catch (IOException e) {
            throw new FhirResourceSearcherException("Error during searching process", e);
        }


        return results;
    }


}

package net.ihe.gazelle.simulator.server.technical.indexers;


import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.hl7.fhir.r4.model.Organization;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class OrganizationResourceIndexer implements FhirResourceIndexer {

    public static final String RESOURCE_NAME = "organization";
    private FhirResourceIndexerService indexerService;

    private final FhirResourceParser parser;


    public OrganizationResourceIndexer(FhirResourceParser parser) {
        this.parser = parser;
        indexerService = new FhirResourceIndexerService(RESOURCE_NAME);
    }

    public OrganizationResourceIndexer() {
        this(new FhirResourceParser());
    }

    @Override
    public void createIndexes(List<String> stringResources) throws IOException {
        for (String stringResource : stringResources) {
            Document doc = new Document();
            Organization organization = this.parser.deserialize(stringResource, Organization.class);
            indexerService.addResource(doc, stringResource);
            indexerService.addIndex(doc, FhirResourceIndexerService.ID, organization.getIdPart());
            indexerService.addIndex(doc, FhirResourceIndexerService.IS_ACTIVE, Boolean.toString(organization.getActive()));
            indexerService.addIdentifier(doc, organization.getIdentifier());
            indexerService.addName(doc, organization.getName());
            indexerService.addReference(doc, FhirResourceIndexerService.PART_OF + FhirResourceIndexerService.ORGANIZATION_REFERENCE, organization.getPartOf());
            indexerService.addType(doc, organization.getType());
            indexerService.addDate(doc, FhirResourceIndexerService.LAST_UPDATED, organization.getMeta().getLastUpdated());
            indexerService.addDocument(doc);
        }
        indexerService.closeIndexer();
    }

    @Override
    public Analyzer getAnalyzer() {
        return indexerService.getAnalyzer();
    }

    @Override
    public Directory getDirectory() {
        return indexerService.getDirectory();
    }

    @Override
    public IndexSearcher getSearcher() {
        return indexerService.getIndexSearcher();
    }

    @Override
    public Map<String, String> getMapOfIndexes() {
        return indexerService.getUsedIndexes();
    }

}

package net.ihe.gazelle.simulator.server.technical.providers.ch;

import net.ihe.gazelle.simulator.server.technical.indexers.PractitionerRoleResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParserCH;
import net.ihe.gazelle.simulator.server.technical.providers.ihe.IHEITI90PractitionerRoleProvider;

public class CHITI90PractitionerRoleProvider extends IHEITI90PractitionerRoleProvider {

    public CHITI90PractitionerRoleProvider() {
        super(new PractitionerRoleResourceIndexer(new FhirResourceParserCH()));
    }

}

package net.ihe.gazelle.simulator.server.technical.providers.ihe;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import net.ihe.gazelle.simulator.server.business.ResourceService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.dao.ResourceServiceDAOJson;
import net.ihe.gazelle.simulator.server.technical.services.FhirResourceService;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.OrganizationAffiliation;

import java.util.List;

public class IHEITI90OrganizationAffiliationProvider implements IResourceProvider {

    ResourceService resourceService;
    ResourceServiceDAO dao;
    public static final String RESOURCE_TYPE = "OrganizationAffiliation";


    public IHEITI90OrganizationAffiliationProvider() {
        this.dao = new ResourceServiceDAOJson(RESOURCE_TYPE, null); // not implemented
        this.resourceService = new FhirResourceService(dao, RESOURCE_TYPE);
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return OrganizationAffiliation.class;
    }

    @Read
    public IBaseResource read(@IdParam IdType theId) {
        IBaseResource resource = resourceService.getResourceById(theId.toString());
        if (resource == null) {
            throw new ResourceNotFoundException(theId);
        }
        return resource;
    }

    @Search
    public List<IBaseResource> search() {
        return resourceService.getAllResources();
    }
}

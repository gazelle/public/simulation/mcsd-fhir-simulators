package net.ihe.gazelle.simulator.server.technical.io;

import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;

public class FhirResourceParserCH extends FhirResourceParser{

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static final XmlMapper xmlMapper = new XmlMapper();


    protected <T extends IBaseResource> T parseTheContent(String input, Class<T> resourceClass, IParser parser) {
        T resource = super.parseTheContent(input, resourceClass, parser);
        if(isActiveNull(input)){
            if(resource instanceof Organization organization){
                organization.setActive(true);
                return (T) organization;
            }
            if(resource instanceof Practitioner practitioner){
                practitioner.setActive(true);
                return (T) practitioner;
            }
            if(resource instanceof PractitionerRole practitionerRole){
                practitionerRole.setActive(true);
                return (T) practitionerRole;
            }
        }
        return resource;
    }

    private boolean isActiveNull(String input){
        JsonNode jsonNode;
        try {
            jsonNode = objectMapper.readTree(input);
        } catch (JsonProcessingException e) {
            try {
                jsonNode = xmlMapper.readTree(input);
            } catch (JsonProcessingException jsonProcessingException) {
                throw new DataFormatException("Content is not a valid format: " + jsonProcessingException.getMessage());
            }
        }
        return jsonNode.get("active") == null;
    }
}

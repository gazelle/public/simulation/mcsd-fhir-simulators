package net.ihe.gazelle.simulator.server.technical.indexers;


import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Endpoint;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class EndpointResourceIndexer implements FhirResourceIndexer {


    public static final String RESOURCE_NAME = "endpoint";
    private FhirResourceIndexerService indexerService;

    private final FhirResourceParser parser;

    public EndpointResourceIndexer(FhirResourceParser parser) {
        this.parser = parser;
        indexerService = new FhirResourceIndexerService(RESOURCE_NAME);
    }

    public EndpointResourceIndexer() {
        this(new FhirResourceParser());
    }

    @Override
    public void createIndexes(List<String> stringResources) throws IOException {
        for (String stringResource : stringResources) {
            Document doc = new Document();
            Endpoint endpoint = this.parser.deserialize(stringResource, Endpoint.class);
            indexerService.addIndex(doc, FhirResourceIndexerService.ID, endpoint.getIdPart());
            indexerService.addResource(doc, stringResource);
            indexerService.addReference(doc, FhirResourceIndexerService.ORGANIZATION_REFERENCE, endpoint.getManagingOrganization());
            indexerService.addIdentifier(doc, endpoint.getIdentifier());
            indexerService.addStatus(doc, endpoint.getStatus().getDisplay());
            indexerService.addDate(doc, FhirResourceIndexerService.LAST_UPDATED, endpoint.getMeta().getLastUpdated());
            indexerService.addDocument(doc);
        }
        indexerService.closeIndexer();
    }

    @Override
    public Analyzer getAnalyzer() {
        return indexerService.getAnalyzer();
    }

    @Override
    public Directory getDirectory() {
        return indexerService.getDirectory();
    }

    @Override
    public IndexSearcher getSearcher() {
        return indexerService.getIndexSearcher();
    }

    @Override
    public Map<String, String> getMapOfIndexes() {
        return indexerService.getUsedIndexes();
    }

}

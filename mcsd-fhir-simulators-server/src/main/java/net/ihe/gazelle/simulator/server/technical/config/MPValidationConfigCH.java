package net.ihe.gazelle.simulator.server.technical.config;


import net.ihe.gazelle.simulator.server.business.config.ValidationConfig;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

import java.util.HashMap;
import java.util.Map;

public class MPValidationConfigCH implements ValidationConfig {

    private final Config config;

    private static final Map<String, Object> properties = new HashMap<>();

    public MPValidationConfigCH() {
        this.config = ConfigProvider.getConfig();
    }

    public String getHttpIti90SearchPractitioner() {
        return getProperty("mcsd.server.validation.profile-iti-90-practitioner");
    }

    public String getHttpIti90SearchPractitionerRole() {
        return getProperty("mcsd.server.validation.profile-iti-90-practitionerRole");
    }

    public String getHttpIti90SearchOrganization() {
        return getProperty("mcsd.server.validation.profile-iti-90-organization");
    }

    public String getHttpValidatorName() {
        return getProperty("mcsd.server.validation.name");
    }

    public String getHttpIti90Read() {
        return getProperty("mcsd.server.validation.profile-iti-90-read");
    }

    public boolean isValidationEnabled() {
        return getProperty("mcsd.server.validation.enabled", boolean.class);
    }

    private <T> T getProperty(String key, Class<T> clazz) {
        properties.putIfAbsent(key, config.getValue(key, clazz));
        return (T) properties.get(key);
    }

    private String getProperty(String key) {
        return getProperty(key, String.class);
    }
}

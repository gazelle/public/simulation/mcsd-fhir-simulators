package net.ihe.gazelle.simulator.server.technical.indexers;

import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FhirResourceIndexerService {

    public static final String ID = "_id";
    public static final String IDENTIFIER = "_identifier";
    public static final String IDENTIFIER_SYSTEM = "_identifier_system";
    public static final String IDENTIFIER_VALUE = "_identifier_value";
    public static final String FAMILY_NAME = "_family_name";
    public static final String GIVEN_NAME = "_given_name";
    public static final String TYPE = "_type";
    public static final String SYSTEM = "_system";
    public static final String CODE = "_code";
    public static final String STATUS = "_status";
    public static final String RESOURCE = "_resource";
    public static final String IS_ACTIVE = "_active";
    public static final String NAME = "_name";
    public static final String HUMAN_NAME = "_human_name";
    public static final String ORGANIZATION_REFERENCE = "_organization_reference";
    public static final String PART_OF = "_partof";
    public static final String PRACTITIONER_REFERENCE = "_practitioner_reference";
    private static final String PIPE = "|";
    public static final String IGNORE_CASE = "_ic";
    public static final String LAST_UPDATED = "_lastUpdated";
    public static final String SPECIALTY = "_specialty";

    Logger log = LoggerFactory.getLogger(FhirResourceIndexerService.class);

    Analyzer analyzer;
    Directory directory;
    IndexWriterConfig config;
    IndexWriter indexWriter;
    String resourceName;
    Map<String, String> usedIndexes;



    public FhirResourceIndexerService(String resourceName) {
        this.resourceName = resourceName;
        usedIndexes = new HashMap<>();
        configureIndexer();
    }


    public void configureIndexer() {
        try {
            analyzer = new KeywordAnalyzer();
            config = new IndexWriterConfig(analyzer);
            directory = FSDirectory.open(Files.createTempDirectory("tempIndex" + resourceName));
            indexWriter = new IndexWriter(directory, config);
        } catch (Exception e) {
            log.error("Error during indexation: {}", e.getMessage());
        }
    }


    void addIndex(Document doc, String fieldName, String value) {
        final String RESOURCE_FIELD_NAME = resourceName + fieldName;
        if (isNotNullOrBlank(value)) {
            doc.add(new TextField(RESOURCE_FIELD_NAME, value, Field.Store.YES));
            addToMapIfNotExist(fieldName, RESOURCE_FIELD_NAME);
        }
    }

    void addIgnoreCaseIndex(Document doc, String fieldName, String value) {
        final String RESOURCE_FIELD_NAME = resourceName + fieldName + IGNORE_CASE;
        if (isNotNullOrBlank(value)) {
            doc.add(new TextField(RESOURCE_FIELD_NAME, value.toLowerCase(), Field.Store.YES));
            addToMapIfNotExist(fieldName + IGNORE_CASE, RESOURCE_FIELD_NAME);
        }
    }

    void addIdentifier(Document doc, List<Identifier> identifiers) {
        if (identifiers != null && !identifiers.isEmpty()) {
            for (Identifier identifier : identifiers) {
                addIndex(doc, IDENTIFIER, identifier.getSystem() + PIPE + identifier.getValue());
                addIndex(doc, IDENTIFIER_SYSTEM, identifier.getSystem());
                addIndex(doc, IDENTIFIER_VALUE, identifier.getValue());
            }
        }
    }


    void addReference(Document doc, String fieldName, Reference referenceObject) {
        String reference = referenceObject.getReference();
        if (isNotNullOrBlank(reference)) {
            String indexValue = reference;

            if (reference.contains("/")) {
                indexValue = reference.substring(reference.indexOf("/") + 1);
            } else if (fieldName.contains(ORGANIZATION_REFERENCE)) {
                indexValue = "Organization/" + reference;
            } else if (fieldName.contains(PRACTITIONER_REFERENCE)) {
                indexValue = "Practitioner/" + reference;
            }

            if (!reference.equals(indexValue)) addIndex(doc, fieldName, indexValue);
            addIndex(doc, fieldName, reference);
        }
    }

    void addName(Document doc, String name) {
                addIndex(doc, NAME, name);
                addIgnoreCaseIndex(doc, NAME, name);
    }


    void addHumanName(Document doc, List<HumanName> names) {
        if (names != null && !names.isEmpty()) {
            for (HumanName name : names) {
                String familyName = name.getFamily();
                addIndex(doc, FAMILY_NAME, familyName);
                addIgnoreCaseIndex(doc, FAMILY_NAME, familyName);
                addIndex(doc, HUMAN_NAME, familyName);
                addIgnoreCaseIndex(doc, HUMAN_NAME, familyName);
                addGivenName(doc, name.getGiven());
            }
        }
    }

    private void addGivenName(Document doc, List<StringType> givenNames) {
        if (givenNames != null && !givenNames.isEmpty()) {
            for (StringType given : givenNames) {
                String givenNameAsString = given.asStringValue();
                addIgnoreCaseIndex(doc, GIVEN_NAME, givenNameAsString);
                addIndex(doc, GIVEN_NAME, givenNameAsString);
                addIndex(doc, HUMAN_NAME, givenNameAsString);
                addIgnoreCaseIndex(doc, HUMAN_NAME, givenNameAsString);
            }
        }
    }

    void addType(Document doc, List<CodeableConcept> codeableConcepts) {
        if (codeableConcepts != null && !codeableConcepts.isEmpty()) {
            for (CodeableConcept codeableConcept : codeableConcepts) {
                addCoding(doc, TYPE, codeableConcept.getCoding());
            }
        }
    }

    void addRole(Document doc, List<CodeableConcept> codeableConcepts) {
        if (codeableConcepts != null && !codeableConcepts.isEmpty()) {
            for (CodeableConcept codeableConcept : codeableConcepts) {
                addCoding(doc, CODE, codeableConcept.getCoding());
            }
        }
    }

    void addSpecialty(Document doc, List<CodeableConcept> codeableConcepts) {
        if (codeableConcepts != null && !codeableConcepts.isEmpty()) {
            for (CodeableConcept codeableConcept : codeableConcepts) {
                addCoding(doc, SPECIALTY, codeableConcept.getCoding());
            }
        }
    }

    private void addCoding(Document doc, String fieldname, List<Coding> codings) {
        if (codings != null && !codings.isEmpty()) {
            for (Coding coding : codings) {
                addIndex(doc, fieldname, String.format("%s%s%s", coding.getSystem(), PIPE, coding.getCode()));
                addIndex(doc, fieldname + SYSTEM, coding.getSystem());
                addIndex(doc, fieldname + CODE, coding.getCode());
            }
        }
    }

    void addDate(Document doc, String fieldName, Date date) {
        if (date != null) {
            final String RESOURCE_FIELD_NAME = resourceName + fieldName;
            doc.add(new LongPoint(RESOURCE_FIELD_NAME, date.getTime()));
            addToMapIfNotExist(fieldName, RESOURCE_FIELD_NAME);
        }
    }

    void addStatus(Document doc, String status) {
        addIndex(doc, STATUS, status);
    }

    private boolean isNotNullOrBlank(String value) {
        return value != null && !value.isBlank();
    }

    public IndexSearcher getIndexSearcher() {
        IndexSearcher is = null;
        try {
            is = new IndexSearcher(DirectoryReader.open(directory));
        } catch (IOException e) {
            log.error("Error while opening DirectoryReader", e);
        }
        return is;
    }

    public void addDocument(Document doc) throws IOException {
        indexWriter.addDocument(doc);
        indexWriter.commit();
    }

    public void addResource(Document doc, String stringResource) {
        addIndex(doc, RESOURCE, stringResource);
    }

    public void closeIndexer() throws IOException {
        indexWriter.close();
    }


    private void addToMapIfNotExist(String key, String fieldName) {
        usedIndexes.computeIfAbsent(key, k -> fieldName);
    }

    public Analyzer getAnalyzer() {
        return analyzer;
    }

    public Directory getDirectory() {
        return directory;
    }

    public Map<String, String> getUsedIndexes() {
        return usedIndexes;
    }


}

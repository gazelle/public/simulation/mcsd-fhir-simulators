package net.ihe.gazelle.simulator.server.business;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.param.*;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ResourceService {

    IBaseResource getResourceById(String id);

    List<IBaseResource> getAllResources();

    List<IBaseResource> processSearchByParams(Map<String, BaseParam> params) throws FhirResourceServiceException;

    List<IBaseResource> getResourcesFromParams(Map<String, BaseParam> params) throws FhirResourceServiceException;

    void processSearchByStringParam(String theField, StringParam theParam) throws FhirResourceSearcherException;

    void processSearchByTokenParam(String theField, TokenParam theParam) throws FhirResourceSearcherException;

    void processSearchByReferenceParam(String theField, ReferenceParam theParam) throws FhirResourceSearcherException;

    void processSearchByDateParam(String theField, DateParam theParam) throws FhirResourceServiceException;

    void setResourcesFromIncludes(List<IBaseResource> resources, Set<Include> theIncludes);

    default void processEntries(Map.Entry<String, BaseParam> entry) throws FhirResourceSearcherException, FhirResourceServiceException {

        String key = entry.getKey();
        BaseParam value = entry.getValue();

        if (value instanceof StringParam param) {
            processSearchByStringParam(key, param);
        }

        if (value instanceof TokenParam param) {
            processSearchByTokenParam(key, param);
        }

        if (value instanceof DateParam param) {
            processSearchByDateParam(key, param);
        }

        if (value instanceof ReferenceParam param) {
            processSearchByReferenceParam(key, param);
        }

    }


}

package net.ihe.gazelle.simulator.server.technical.services;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.param.*;
import net.ihe.gazelle.simulator.server.business.ResourceService;
import net.ihe.gazelle.simulator.server.business.ResourceServiceDAO;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceServiceException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class FhirResourceService implements ResourceService {

    Logger log = LoggerFactory.getLogger(FhirResourceService.class);
    private ResourceServiceDAO resourceDao;
    String resourceType;


    public FhirResourceService(ResourceServiceDAO resourceDao, String resourceType) {
        this.resourceDao = resourceDao;
        this.resourceType = resourceType;
    }

    @Override
    public IBaseResource getResourceById(String id) {
        return this.resourceDao.getResourceWithId(id);
    }

    @Override
    public List<IBaseResource> getAllResources() {
        return this.resourceDao.getAllResourcesByResourceType(resourceType);
    }

    @Override
    public List<IBaseResource> processSearchByParams(Map<String, BaseParam> params) throws FhirResourceServiceException {
        List<IBaseResource> resources = List.of();
        try {
            for (Map.Entry<String, BaseParam> entry : params.entrySet()) {
                if (entry.getValue() != null) {
                    processEntries(entry);
                }
            }
            resources = this.resourceDao.getResourcesFromSearch(resourceType);
        } catch (FhirResourceSearcherException e) {
            log.error("Problem during search process: {}", e.getMessage());
        }

        return resources;
    }

    public void processSearchByStringParam(String theField, StringParam theParam) throws FhirResourceSearcherException {
        String nameValue = theParam.getValue();
        String[] valuesToMatch = theField.equals("_human_name")
                ? nameValue.split(" ")
                : new String[]{nameValue};

        for (String value : valuesToMatch) {
            processName(theField, theParam, value);
        }
    }

    void processName(String theField, StringParam theParam, String value) throws FhirResourceSearcherException {
        if (theParam.isExact()) {
            this.resourceDao.queryResourceByExactString(theField, value);
        } else if (theParam.isContains()) {
            this.resourceDao.queryResourceByApproximateString(theField, value);
        } else {
            this.resourceDao.queryResourceByPrefix(theField, value);
        }
    }


    @Override
    public void processSearchByTokenParam(String theField, TokenParam theParam) throws FhirResourceSearcherException {

        if ("_identifier".equals(theField)) {
            queryResourceByIdentifier(theParam);
        } else if ("_specialty".equals(theField)) {
            queryResourceBySpecialty(theParam);
        } else if ("_role".equals(theField)) {
            queryResourceByRole(theParam);
        } else if ("_type".equals(theField)) {
            queryResourceByType(theParam);
        }else {
            this.resourceDao.queryResourceByExactString(theField, theParam.getValue());
        }

    }

    @Override
    public void processSearchByReferenceParam(String theField, ReferenceParam theParam) throws FhirResourceSearcherException {
        this.resourceDao.queryResourceByExactString(theField, theParam.getValue());
    }

    @Override
    public void processSearchByDateParam(String theField, DateParam theParam) throws FhirResourceServiceException {

        String modifier = theParam.getPrefix().getValue();
        long timeIntoMs = theParam.getValue().getTime();

        if (theField.contains("_lastUpdated")) {
            theField = "_lastUpdated";
        }

        switch (modifier) {
            case "eq" -> this.resourceDao.queryResourceByDate(theField, timeIntoMs, timeIntoMs + 86399000L);
            case "ge" -> this.resourceDao.queryResourceByDate(theField, timeIntoMs, Long.MAX_VALUE);
            case "le" -> this.resourceDao.queryResourceByDate(theField, 0L, timeIntoMs + 86399000L);
            case "gt" -> this.resourceDao.queryResourceByDate(theField, timeIntoMs + 86400000L, Long.MAX_VALUE);
            case "lt" -> this.resourceDao.queryResourceByDate(theField, 0L, timeIntoMs - 1L);
            default ->
                    throw new FhirResourceServiceException("Invalid date modifier: " + modifier + "is not supported");
        }
    }

    public void queryResourceByIdentifier(TokenParam theToken) throws FhirResourceSearcherException {
        String identifierSystem = theToken.getSystem() != null ? theToken.getSystem() : "";
        String identifierValue = theToken.getValue() != null ? theToken.getValue() : "";

        if (!identifierSystem.isBlank() && !identifierValue.isBlank()) {
            this.resourceDao.queryResourceByExactString("_identifier", identifierSystem + "|" + identifierValue);
        } else if (!identifierSystem.isBlank()) {
            this.resourceDao.queryResourceByExactString("_identifier_system", identifierSystem);
        } else {
            this.resourceDao.queryResourceByExactString("_identifier_value", identifierValue);
        }

    }

    public void queryResourceBySpecialty(TokenParam theToken) throws FhirResourceSearcherException {
        String specialtySystem = theToken.getSystem() != null ? theToken.getSystem() : "";
        String specialtyCode = theToken.getValue() != null ? theToken.getValue() : "";

        if (!specialtySystem.isBlank() && !specialtyCode.isBlank()) {
            this.resourceDao.queryResourceByExactString("_specialty", specialtySystem + "|" + specialtyCode);
        } else if (!specialtySystem.isBlank()) {
            this.resourceDao.queryResourceByExactString("_specialty_system", specialtySystem);
        } else {
            this.resourceDao.queryResourceByExactString("_specialty_code", specialtyCode);
        }

    }

    public void queryResourceByRole(TokenParam theToken) throws FhirResourceSearcherException {
        String codeSystem = theToken.getSystem() != null ? theToken.getSystem() : "";
        String codeCode = theToken.getValue() != null ? theToken.getValue() : "";

        if (!codeSystem.isBlank() && !codeCode.isBlank()) {
            this.resourceDao.queryResourceByExactString("_code", codeSystem + "|" + codeCode);
        } else if (!codeSystem.isBlank()) {
            this.resourceDao.queryResourceByExactString("_code_system", codeSystem);
        } else {
            this.resourceDao.queryResourceByExactString("_code_code", codeCode);
        }

    }

    public void queryResourceByType (TokenParam theToken) throws FhirResourceSearcherException {
        String typeSystem = theToken.getSystem() != null ? theToken.getSystem() : "";
        String typeCode = theToken.getValue() != null ? theToken.getValue() : "";

        if (!typeSystem.isBlank() && !typeCode.isBlank()) {
            this.resourceDao.queryResourceByExactString("_type", typeSystem + "|" + typeCode);
        } else if (!typeSystem.isBlank()) {
            this.resourceDao.queryResourceByExactString("_type_system", typeSystem);
        } else {
            this.resourceDao.queryResourceByExactString("_type_code", typeCode);
        }

    }

    public List<IBaseResource> getResourcesFromParams(Map<String, BaseParam> params) throws FhirResourceServiceException {
        List<IBaseResource> resources;

        if (params.isEmpty()) {
            resources = getAllResources();
        } else {
            resources = processSearchByParams(params);
        }

        return resources;
    }

    @Override
    public void setResourcesFromIncludes(List<IBaseResource> resources, Set<Include> theIncludes) {
        if (theIncludes.contains(new Include("PractitionerRole:practitioner"))) {
            for (IBaseResource resource : resources) {
                PractitionerRole practitionerRole = (PractitionerRole) resource;

                if (practitionerRole.getPractitioner() != null) {
                    String reference = practitionerRole.getPractitioner().getReference();
                    IBaseResource practitioner = resourceDao.getResourceWithId(reference);
                    practitionerRole.getPractitioner().setResource(practitioner);
                }
            }
        }

    }

}

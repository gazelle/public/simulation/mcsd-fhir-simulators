package net.ihe.gazelle.simulator.server.technical.rest;

import ca.uhn.fhir.rest.server.FifoMemoryPagingProvider;
import ca.uhn.fhir.rest.server.HardcodedServerAddressStrategy;
import jakarta.enterprise.inject.Alternative;
import net.ihe.gazelle.fhir.server.technical.HTTPValidatorInterceptor;
import net.ihe.gazelle.simulator.server.business.MCSDResourcesProvidersFactory;
import net.ihe.gazelle.simulator.server.business.config.OperationalConfig;
import net.ihe.gazelle.simulator.server.business.config.ValidationConfig;
import net.ihe.gazelle.simulator.server.technical.config.MPOperationalConfig;
import net.ihe.gazelle.simulator.server.technical.config.MPValidationConfigCH;
import net.ihe.gazelle.simulator.server.technical.factory.providers.MCSDResourceProvidersFactoryProviderImpl;
import net.ihe.gazelle.simulator.server.technical.io.mapper.MCSDHttpValidationServiceMapper;
import org.slf4j.Logger;

import java.io.Serial;
import java.util.List;

@Alternative
public class CHITI90RestfulServer extends IHEITI90RestfulServer {

    @Serial
    private static final long serialVersionUID = 1L;

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(CHITI90RestfulServer.class);

    private final transient OperationalConfig operationalConfig = new MPOperationalConfig();

    private final transient ValidationConfig validationConfig = new MPValidationConfigCH();

    @Override
    protected void initialize() {
        initializeProivders();
        intializePaging();
        initializeServerAddressStrategy();
        initializeInterceptorForValidation();
    }

    void initializeProivders() {
        List<MCSDResourcesProvidersFactory> providerFactories = getProvidersFactories(MCSDResourceProvidersFactoryProviderImpl.CH_PROFILE);
        setResourceProviders(initiateListOfProvidersForServlet(providerFactories));
    }

    void intializePaging() {
        FifoMemoryPagingProvider pp = new FifoMemoryPagingProvider(10);
        pp.setDefaultPageSize(10);
        pp.setMaximumPageSize(100);
        setPagingProvider(pp);
    }

    void initializeServerAddressStrategy() {
        String serverBaseUrl = operationalConfig.getServerBaseUrl("ch");
        setServerAddressStrategy(new HardcodedServerAddressStrategy(serverBaseUrl));
    }

    void initializeInterceptorForValidation() {
        if (validationConfig.isValidationEnabled()) {
            logger.info("Validation is enabled for CH ITI-90");
            registerInterceptor(
                    new HTTPValidatorInterceptor(operationalConfig.getEvsEndpoint(), new MCSDHttpValidationServiceMapper(validationConfig))
            );
        }
    }

}

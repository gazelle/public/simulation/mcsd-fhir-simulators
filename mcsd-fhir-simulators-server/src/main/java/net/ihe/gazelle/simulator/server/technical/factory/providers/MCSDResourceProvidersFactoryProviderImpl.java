package net.ihe.gazelle.simulator.server.technical.factory.providers;

import net.ihe.gazelle.simulator.server.business.MCSDResourceProvidersFactoryProvider;
import net.ihe.gazelle.simulator.server.business.MCSDResourcesProvidersFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class MCSDResourceProvidersFactoryProviderImpl implements MCSDResourceProvidersFactoryProvider {

    public static final String IHE_PROFILE = "IHE";
    public static final String CH_PROFILE = "CH";

    public List<MCSDResourcesProvidersFactory> createListOfMCSDResourceProviders(String profile) {
        Iterable<MCSDResourcesProvidersFactory> allMCSDProviders = getListOfAllMCSDResourceProviders();
        List<MCSDResourcesProvidersFactory> listOfMCSDResourcesProviders = new ArrayList<>();

        for (MCSDResourcesProvidersFactory mCSDResourceProvidersFactory : allMCSDProviders) {
            if (mCSDResourceProvidersFactory.hasForProfile(profile)) {
                listOfMCSDResourcesProviders.add(mCSDResourceProvidersFactory);
            }
        }
        return listOfMCSDResourcesProviders;
    }

    public Iterable<MCSDResourcesProvidersFactory> getListOfAllMCSDResourceProviders() {
        return ServiceLoader.load(MCSDResourcesProvidersFactory.class);
    }

}

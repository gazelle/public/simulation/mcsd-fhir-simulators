package net.ihe.gazelle.simulator.server.technical.io;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceParserException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;


public class FhirResourceParser {

    public static final String XML = "xml";
    public static final String JSON = "json";
    private static final Logger log = getLogger(FhirResourceParser.class);
    private static final FhirContext ctx = FhirContext.forR4();
    private static final IParser jsonParser = ctx.newJsonParser();
    private static final  IParser xmlParser = ctx.newXmlParser();


    public String serialize(IBaseResource resource, String format) {
        if (XML.equalsIgnoreCase(format)) {
            return xmlParser.encodeResourceToString(resource);
        } else if (JSON.equalsIgnoreCase(format)) {
            return jsonParser.encodeResourceToString(resource);
        } else {
            throw new FhirResourceParserException("Format should be 'json' or 'xml'.");
        }
    }

    public <T extends IBaseResource> T deserialize(String input, Class<T> resourceClass) {

        T res = parseTheContent(input, resourceClass, jsonParser);
        if (res == null) {
            res = parseTheContent(input, resourceClass, xmlParser);
        }
        if (res == null) {
            throw new FhirResourceParserException("Content should be a valid JSON or XML.");
        }
        return res;

    }

    protected <T extends IBaseResource> T parseTheContent(String input, Class<T> resourceClass, IParser parser) {
        T res = null;
        try {
            res = parser.parseResource(resourceClass, input);
        } catch (DataFormatException e) {
            log.debug("Content is not a valid format: {}", e.getMessage());
        }
        return res;
    }


}

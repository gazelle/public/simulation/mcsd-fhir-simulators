package net.ihe.gazelle.simulator.server.technical.exception;

public class FhirResourceSearcherException extends Exception {

    public FhirResourceSearcherException(String message, Throwable cause) {
        super(message, cause);
    }

    public FhirResourceSearcherException(String message) {
        super(message);
    }

}

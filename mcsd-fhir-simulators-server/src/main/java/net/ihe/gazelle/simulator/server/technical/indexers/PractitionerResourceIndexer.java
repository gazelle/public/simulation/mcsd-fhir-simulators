package net.ihe.gazelle.simulator.server.technical.indexers;


import net.ihe.gazelle.simulator.server.business.FhirResourceIndexer;
import net.ihe.gazelle.simulator.server.technical.io.FhirResourceParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.hl7.fhir.r4.model.Practitioner;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public class PractitionerResourceIndexer implements FhirResourceIndexer {

    public static final String RESOURCE_NAME = "practitioner";
    private FhirResourceIndexerService indexerService;

    private final FhirResourceParser parser;

    public PractitionerResourceIndexer(FhirResourceParser parser) {
        this.parser = parser;
        indexerService = new FhirResourceIndexerService(RESOURCE_NAME);
    }

    public PractitionerResourceIndexer() {
        this(new FhirResourceParser());
    }

    public void createIndexes(List<String> stringResources) throws IOException {
        for (String stringResource : stringResources) {
            Document doc = new Document();
            indexerService.addResource(doc, stringResource);
            Practitioner practitioner = this.parser.deserialize(stringResource, Practitioner.class);
            indexerService.addIndex(doc, FhirResourceIndexerService.ID, practitioner.getIdPart());
            indexerService.addIndex(doc, FhirResourceIndexerService.IS_ACTIVE, Boolean.toString(practitioner.getActive()));
            indexerService.addIdentifier(doc, practitioner.getIdentifier());
            indexerService.addHumanName(doc, practitioner.getName());
            indexerService.addDate(doc, FhirResourceIndexerService.LAST_UPDATED, practitioner.getMeta().getLastUpdated());
            indexerService.addDocument(doc);
        }
        indexerService.closeIndexer();
    }

    @Override
    public Analyzer getAnalyzer() {
        return indexerService.getAnalyzer();
    }

    @Override
    public Directory getDirectory() {
        return indexerService.getDirectory();
    }

    @Override
    public IndexSearcher getSearcher() {
        return indexerService.getIndexSearcher();
    }

    @Override
    public Map<String, String> getMapOfIndexes() {
        return indexerService.getUsedIndexes();
    }

}

package net.ihe.gazelle.simulator.server.technical.config;

import net.ihe.gazelle.simulator.server.business.config.OperationalConfig;
import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.ConfigProvider;

public class MPOperationalConfig implements OperationalConfig {

    private final Config config;

    private String evsEndpoint;
    private String serverBaseUrl;


    public MPOperationalConfig() {
        this.config = ConfigProvider.getConfig();
    }

    public String getEvsEndpoint() {
        if (evsEndpoint == null) {
            evsEndpoint = config.getValue("mcsd.server.evs.endpoint", String.class);
        }
        return evsEndpoint;
    }

    public String getServerBaseUrl(String profile) {
        if (serverBaseUrl == null) {
            serverBaseUrl = config.getValue("mcsd.server.base.url", String.class) + "/" + profile;
        }
        return serverBaseUrl;
    }


}

package net.ihe.gazelle.simulator.server.technical.io.mapper;

import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import net.ihe.gazelle.evsapi.client.business.ValidationService;
import net.ihe.gazelle.fhir.server.business.ValidatorServiceMapper;
import net.ihe.gazelle.simulator.server.business.config.ValidationConfig;


public class MCSDHttpValidationServiceMapper implements ValidatorServiceMapper {

    private final ValidationConfig validationConfig;

    public MCSDHttpValidationServiceMapper(ValidationConfig validationConfig) {
        this.validationConfig = validationConfig;
    }

    @Override
    public ValidationService getValidationService(RequestDetails requestDetails) {
        String httpValidationName = validationConfig.getHttpValidatorName();
        ValidationService validationService = new ValidationService()
                .setName(httpValidationName);
        if (requestDetails.getRestOperationType().equals(RestOperationTypeEnum.SEARCH_TYPE)) {
            return switch (requestDetails.getResourceName()) {
                case "Practitioner" -> validationService
                        .setValidator(validationConfig.getHttpIti90SearchPractitioner());
                case "PractitionerRole" -> validationService
                        .setValidator(validationConfig.getHttpIti90SearchPractitionerRole());
                case "Organization" -> validationService
                        .setValidator(validationConfig.getHttpIti90SearchOrganization());
                default -> null;
            };
        }
        if(requestDetails.getRestOperationType().equals(RestOperationTypeEnum.READ)) {
            return validationService.setValidator(validationConfig.getHttpIti90Read());
        }
        return null;

    }

}

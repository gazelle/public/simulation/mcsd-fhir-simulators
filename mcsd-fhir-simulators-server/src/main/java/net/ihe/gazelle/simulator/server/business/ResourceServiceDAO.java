package net.ihe.gazelle.simulator.server.business;


import net.ihe.gazelle.simulator.server.technical.exception.FhirResourceSearcherException;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.List;
import java.util.Map;

public interface ResourceServiceDAO {

    IBaseResource getResourceWithId(String id);

    List<IBaseResource> getAllResourcesByResourceType(String resourceType);

    Map<String, String> getIndexes();

    void queryResourceByExactString(String index, String name) throws FhirResourceSearcherException;

    void queryResourceByApproximateString(String index, String name) throws FhirResourceSearcherException;

    void queryResourceByPrefix(String index, String name) throws FhirResourceSearcherException;

    void queryResourceByDate(String index, long queryMin, long queryMax);

    List<IBaseResource> getResourcesFromSearch(String resourceName) throws FhirResourceSearcherException;

}

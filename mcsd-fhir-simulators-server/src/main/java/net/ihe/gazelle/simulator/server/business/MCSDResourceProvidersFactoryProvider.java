package net.ihe.gazelle.simulator.server.business;

import java.util.List;

public interface MCSDResourceProvidersFactoryProvider {

    List<MCSDResourcesProvidersFactory> createListOfMCSDResourceProviders(String profile);

    Iterable<MCSDResourcesProvidersFactory> getListOfAllMCSDResourceProviders();
}

package net.ihe.gazelle.simulator.server.technical.exception;

import ca.uhn.fhir.parser.DataFormatException;

public class FhirResourceParserException extends DataFormatException {

    public FhirResourceParserException(String theMessage) {
        super(theMessage);
    }

}

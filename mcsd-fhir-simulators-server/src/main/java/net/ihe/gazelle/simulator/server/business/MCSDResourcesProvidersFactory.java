package net.ihe.gazelle.simulator.server.business;

import ca.uhn.fhir.rest.server.IResourceProvider;

public interface MCSDResourcesProvidersFactory {

    IResourceProvider createMCSDResourceProviders();

    boolean hasForProfile(String profile);
}

package net.ihe.gazelle.simulator.server.business;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FhirResourceIndexer {

    Analyzer getAnalyzer();

    Directory getDirectory();

    IndexSearcher getSearcher();

    Map<String, String> getMapOfIndexes();

    void createIndexes(List<String> stringResources) throws IOException;

}
